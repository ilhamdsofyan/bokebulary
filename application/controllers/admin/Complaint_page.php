<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint_page extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}

		$this->load->model('admin/Notification','notif');
		$this->load->model('admin/Complaint_model','dataCompaint');
	}

	public function index() {
		$data['jml'] = $this->notif->notify()->num_rows();
		$this->load->view('admin/hater/header', $data);
		$this->load->view('admin/complaint', $data);
	}

	public function ajax_list() {
		$list = $this->dataCompaint->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $complaint) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $complaint->Nama_Mahasiswa;
			$row[] = $complaint->judul;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Show Detail" onclick="show('."".$complaint->id."".')"><i class="glyphicon glyphicon-eye"></i> Show</a>
				    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Fixed" onclick="delete_complaint('."'".$complaint->id."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->dataCompaint->count_all(),
						"recordsFiltered" => $this->dataCompaint->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_show($id) {
		$data = $this->dataCompaint->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_delete($id) {
		$attatch = $this->dataCompaint->getPic($id);
		// print_r($attatch);exit;
		if (isset($attatch[0]['lampiran'])) {
			unlink("./assets/storage/attach/".$attatch[0]['lampiran']);
		}

		$this->dataCompaint->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}

/* End of file complaint_page.php */
/* Location: ./application/controllers/admin/complaint_page.php */