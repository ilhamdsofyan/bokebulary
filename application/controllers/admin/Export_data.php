<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_data extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
			redirect('login/admin','refresh');
		}
		$this->load->model('admin/Notification','notif');
		$this->load->model('admin/Export', 'export');
		$this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index() {
		$data['jml'] = $this->notif->notify()->num_rows();
		$data['cabang'] = $this->export->selectBranch();
		$this->load->view('admin/exim_data', $data);
	}

	/**
	 * [pollege_employs description]
	 * @param  string $table [get Table Name]
	 * @return [type]        [description]
	 */
	public function pollege_employs() {
		ini_set("memory_limit", "500M");
		$kodecabang = $this->input->post('cabang');
		$cabang = array("kodecabang" => $kodecabang);
		$table = $this->input->post('type');

		$field = $this->export->selectField($table);
		$data = $this->export->selectPollege($table, $cabang);
		$name = ucfirst(str_replace("profilkaryawan", "", $table));

		// echo sizeof($data);exit;
		if(sizeof($data) == 0){
			echo "<script>alert('Employers Data is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Personal Data of Employers");

		$objSet = $phpExcel->setActiveSheetIndex(0);
		$objGet = $phpExcel->getActiveSheet();

		$objGet->setTitle("Bio Sheets"); //Sheet Title
		$objGet->getStyle("A1:AL1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);

		//Set Table Header
		$column = array(
						"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL"
					); //ActiveColumn
		//Field Name
		foreach ($field as $key) {
			$colValue[] = $key->name;
		}

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($data as $key) {
			$objSet->setCellValue("A".$start, $start-1);

			for ($i=1; $i < sizeof($column); $i++) { 
				$objSet->setCellValue($column[$i].$start, $key[$field[$i]->name]);
			}

			$phpExcel->getActiveSheet()->getStyle('AL1:AL'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$phpExcel->getActiveSheet()->setTitle("Data Exported");
		$phpExcel->setActiveSheetIndex(0);

		$filename = urlencode("$name"."Data_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	/**
	 * [lcc_employs description]
	 * @return [type] [description]
	 */
	public function lcc_employs() {
		ini_set("memory_limit", "500M");
		$table = $this->input->post('type');

		$field = $this->export->selectField($table);
		$data = $this->export->selectData($table);
		$name = ucfirst(str_replace("profilkaryawan", "", $table));

		// echo sizeof($data);exit;
		if(sizeof($data) == 0){
			echo "<script>alert('Employers Data is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Personal Data of Employers");

		$objSet = $phpExcel->setActiveSheetIndex(0);
		$objGet = $phpExcel->getActiveSheet();

		$objGet->setTitle("Bio Sheets"); //Sheet Title
		$objGet->getStyle("A1:G1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);

		//Set Table Header
		$column = array(
						"A","B","C","D","E","F","G"
					); //ActiveColumn
		//Field Name
		foreach ($field as $key) {
			$colValue[] = $key->name;
		}

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($data as $key) {
			$objSet->setCellValue("A".$start, $start-1);

			for ($i=1; $i < sizeof($column); $i++) { 
				$objSet->setCellValue($column[$i].$start, $key[$field[$i]->name]);
			}

			$phpExcel->getActiveSheet()->getStyle('G1:G'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$phpExcel->getActiveSheet()->setTitle("Data Exported");
		$phpExcel->setActiveSheetIndex(0);

		$filename = urlencode("$name"."Data_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	/**
	 * [exportStudents description]
	 * @return [type] [description]
	 */
	public function students() {
		ini_set("memory_limit", "500M");
		$kodecabang = $this->input->post('cabang');
		$cabang = array("kodecabang" => $kodecabang);

		$field = $this->export->selectField("biodata");
		$data = $this->export->selectStudentData($cabang);
		// echo sizeof($data);exit;
		if(sizeof($data) == 0){
			echo "<script>alert('Students Data is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Personal Data of Students");

		$objSet = $phpExcel->setActiveSheetIndex(0);
		$objGet = $phpExcel->getActiveSheet();

		$objGet->setTitle("Bio Sheets"); //Sheet Title
		$objGet->getStyle("A1:Y1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);

		//Set Table Header
		$column = array(
						"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y"
					);//ActiveColumn
		//Field Name
		foreach ($field as $key) {
			$colValue[] = $key->name;
		}

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($data as $key) {
			$objSet->setCellValue("A".$start, $start-1);

			for ($i=1; $i < sizeof($column); $i++) { 
				$objSet->setCellValue($column[$i].$start, $key[$field[$i]->name]);
			}

			$phpExcel->getActiveSheet()->getStyle('Y1:Y'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$phpExcel->getActiveSheet()->setTitle("Data Exported");
		$phpExcel->setActiveSheetIndex(0);

		$filename = urlencode("StudentsData_".$kodecabang."_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	/**
	 * [export_student_score description]
	 * @return [type] [description]
	 */
	public function student_score() {
		ini_set("memory_limit", "500M");
		$kodecabang = $this->input->post('cabang');
		$cabang = array("kodecabang" => $kodecabang);

		$dataTest = $this->export->selectStudentTest($cabang);
		$dataReal = $this->export->selectStudentReal($cabang);
		
		if(sizeof($dataTest) == 0 && sizeof($dataReal) == 0){
			echo "<script>alert('Students Score is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Score of Students");

		//Start Vocabulary Test//
		$objSet = $phpExcel->createSheet(0);

		$objSet->getStyle("A1:E1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);
		//Set Table Header
		$column = array("A","B","C","D","E"); //ActiveColumn
		//Field Name
		$colValue = ["No", "NIM", "Name", "Level", "Score"];

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($dataTest as $key) {
			$objSet->setCellValue("A".$start, $start-1);
			$objSet->setCellValue("B".$start, $key['nim']);
			$objSet->setCellValue("C".$start, $key['Nama_Mahasiswa']);
			$objSet->setCellValue("D".$start, $key['smt']);
			$objSet->setCellValue("E".$start, $key['nilai']." Point");

			$phpExcel->getActiveSheet()->getStyle('E1:E'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$objSet->setTitle("Vocabulary Test"); //Sheet Title
		//End Vocabulary Test//
		//Start Vocabulary Real Test//
		$objSet = $phpExcel->createSheet(1);
		
		$objSet->getStyle("A1:E1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);
		//Set Table Header
		$column = array("A","B","C","D","E"); //ActiveColumn
		//Field Name
		$colValue[] = array("No", "NIM", "Name", "Level", "Score");

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($dataReal as $key) {
			$objSet->setCellValue("A".$start, $start-1);
			$objSet->setCellValue("B".$start, $key['nim']);
			$objSet->setCellValue("C".$start, $key['Nama_Mahasiswa']);
			$objSet->setCellValue("D".$start, $key['smt']);
			$objSet->setCellValue("E".$start, $key['nilai']." Point");

			$phpExcel->getActiveSheet()->getStyle('E1:E'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$objSet->setTitle("Vocabulary Real Test"); //Sheet Title
		//End Vocabulary Test//
		
		$filename = urlencode("StudentsScore_".$kodecabang."_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');
	}

	/**
	 * [export_employ_score description]
	 * @return [type] [description]
	 */
	public function employ_score() {
		ini_set("memory_limit", "500M");
		$table = $this->input->post('tipe');
		$kodecabang = $this->input->post('cabang');
		$cabang = array("kodecabang" => $kodecabang);

		$getName = ucfirst(str_replace("profilkaryawan", "", $table));

		$dataTest = $this->export->selectEmployTest($table, $cabang);
		$dataReal = $this->export->selectEmployReal($table, $cabang);
		
		if(sizeof($dataTest) == 0 && sizeof($dataReal) == 0){
			echo "<script>alert('Employers Score is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Score of Employers");

		//Start Vocabulary Test//
		$objSet = $phpExcel->createSheet(0);

		$objSet->getStyle("A1:E1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);
		//Set Table Header
		$column = array("A","B","C","D","E"); //ActiveColumn
		//Field Name
		$colValue = ["No", "NIK", "Name", "Level", "Score"];

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($dataTest as $key) {
			$objSet->setCellValue("A".$start, $start-1);
			$objSet->setCellValue("B".$start, $key['nik']);
			$objSet->setCellValue("C".$start, $key['nama']);
			$objSet->setCellValue("D".$start, $key['smt']);
			$objSet->setCellValue("E".$start, $key['nilai']." Point");

			$phpExcel->getActiveSheet()->getStyle('E1:E'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$objSet->setTitle("Vocabulary Test"); //Sheet Title
		//End Vocabulary Test//
		//Start Vocabulary Real Test//
		$objSet = $phpExcel->createSheet(1);
		
		$objSet->getStyle("A1:E1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);
		//Set Table Header
		$column = array("A","B","C","D","E"); //ActiveColumn
		//Field Name
		$colValue[] = array("No", "NIM", "Name", "Level", "Score");

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($dataReal as $key) {
			$objSet->setCellValue("A".$start, $start-1);
			$objSet->setCellValue("B".$start, $key['nik']);
			$objSet->setCellValue("C".$start, $key['nama']);
			$objSet->setCellValue("D".$start, $key['smt']);
			$objSet->setCellValue("E".$start, $key['nilai']." Point");

			$phpExcel->getActiveSheet()->getStyle('E1:E'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$objSet->setTitle("Vocabulary Real Test"); //Sheet Title
		//End Vocabulary Test//
		
		$filename = urlencode("".$getName."Score_".$kodecabang."_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');
	}

}
/* End of file Export_import_data.php */
/* Location: ./application/controllers/admin/Export_import_data.php */