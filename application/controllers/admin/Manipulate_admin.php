<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manipulate_admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('admin/Manipulate', 'manipulate');
	}

	public function ajax_update() {
		$key = $this->input->post('username');
		$old = md5($this->input->post('old_pass'));
		$conf = md5($this->input->post('conf_pass'));

		$data = array("password" => $conf);
		$validate = array(
					"username" => $key,
					"password" => $old);

		if ($this->manipulate->update($validate, $data) != 0) {
			echo json_encode(array("status" => TRUE));
		}
		else{
			echo json_encode(array("status" => FALSE));
		}
	}

}

/* End of file Manipulate_admin.php */
/* Location: ./application/controllers/admin/Manipulate_admin.php */