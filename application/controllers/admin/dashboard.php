<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}
			$this->load->model('admin/Notification','notif');
		}
	
		public function index() {
			$data['jml'] = $this->notif->notify()->num_rows();
			$this->load->view('admin/hater/header', $data);
			$this->load->view('admin/dashboard', $data);
		}
	
	}
	
	/* End of file dashboard.php */
	/* Location: ./application/controllers/admin/dashboard.php */
?>