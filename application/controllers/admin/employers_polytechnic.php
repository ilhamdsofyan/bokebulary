<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Employers_polytechnic extends CI_Controller {
	
		public function __construct() {
			parent::__construct();
			if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}
			$this->load->model('admin/polytechnic_model','crud');
			$this->load->model('admin/Notification','notif');
			$this->countComplaint = $this->notif->notify()->num_rows();
		}
	
		public function index() {
			$data['cabang'] = $this->crud->getBranch();
			$data['jml'] = $this->countComplaint;
			$this->load->view('admin/polytechnic',$data);
		}

		public function ajax_list() {
			$list = $this->crud->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $employ) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $employ->namacabang;
				$row[] = $employ->nik;
				$row[] = $employ->nama;
				$row[] = $employ->tanggal;

				//add html for action
				$row[] = '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_polytechnic('."".$employ->id_pol."".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
					    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_polytechnic('."'".$employ->id_pol."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->crud->count_all(),
							"recordsFiltered" => $this->crud->count_filtered(),
							"data" => $data,
					);
			//output to json format
			echo json_encode($output);
		}

		public function ajax_edit($id) {
		$data = $this->crud->get_by_id($id);
		echo json_encode($data);
		}

		public function ajax_add() {
			$data = array(
				'nik' => $this->input->post('nik'),
				'nama' => $this->input->post('nama'),
				'kodecabang' => $this->input->post('kodecabang'),
				'alamat' => $this->input->post('alamat'),
				'tempat' => $this->input->post('tempat'),
				'tanggal' => $this->input->post('tanggal'),
				'email' => $this->input->post('email'),
				'tglbergabung' => $this->input->post('tglbergabung')
			);
			$insert = $this->crud->save($data);
			echo json_encode(array("status" => TRUE));
		}

		public function ajax_update() {
			$data = array(
				'nik' => $this->input->post('nik'),
				'nama' => $this->input->post('nama'),
				'kodecabang' => $this->input->post('kodecabang'),
				'alamat' => $this->input->post('alamat'),
				'tempat' => $this->input->post('tempat'),
				'tanggal' => $this->input->post('tanggal'),
				'email' => $this->input->post('email'),
				'tglbergabung' => $this->input->post('tglbergabung')
			);
			$this->crud->update(array('id_pol' => $this->input->post('id_pol')), $data);
			echo json_encode(array("status" => TRUE));
		}

		public function ajax_delete($id) {
			$this->crud->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}
	
	}
	
	/* End of file employers_polytechnic.php */
	/* Location: ./application/controllers/admin/employers_polytechnic.php */
?>