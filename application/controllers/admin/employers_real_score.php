<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employers_real_score extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}
		$this->load->model('admin/employ_real','score');
		$this->load->model('admin/Notification','notif');
		$this->countComplaint = $this->notif->notify()->num_rows();
	}

	public function index() {
			$data['jml'] = $this->countComplaint;
			$data['cabang'] = $this->score->getBranch();
			$this->load->view('admin/employer_real',$data);
		}

		public function ajax_list($from,$where = null) {
			$list = $this->score->get_datatables($from,$where);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $scores) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $scores->nik;
				$row[] = $scores->nama;
				$row[] = $scores->smt;
				$row[] = $scores->terjawab." / 500";
				$row[] = $scores->nilai." Points";
			
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->score->count_all($from, $where),
							"recordsFiltered" => $this->score->count_filtered($from,$where),
							"data" => $data,
					);
			//output to json format
			echo json_encode($output);
		}

}

/* End of file employers_score.php */
/* Location: ./application/controllers/admin/employers_score.php */