<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Questions extends CI_Controller {
		public function __construct() {
			parent::__construct();
			if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}
			$this->load->model('admin/question_model','crud');
			$this->load->model('admin/Notification','notif');
			$this->countComplaint = $this->notif->notify()->num_rows();
		}
	
		public function index() {
			$data['jml'] = $this->countComplaint;
			$this->load->view('admin/question',$data);
		}

		public function ajax_list() {
			$list = $this->crud->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $quest) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = 'Level '.$quest->smt;
				$row[] = $quest->soal;
				$row[] = $quest->jawaban;

				//add html for action
				$row[] = '<a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_quest('."'".$quest->idsoal."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
					  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_quest('."'".$quest->idsoal."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->crud->count_all(),
							"recordsFiltered" => $this->crud->count_filtered(),
							"data" => $data,
					);
			//output to json format
			echo json_encode($output);
		}

		public function ajax_edit($id) {
		$data = $this->crud->get_by_id($id);
		echo json_encode($data);
		}

		public function ajax_add() {
			$data = array(
				'smt' => $this->input->post('smt'),
				'soal' => $this->input->post('soal'),
				'jawaban' => $this->input->post('jawaban'),
			);
			$insert = $this->crud->save($data);
			echo json_encode(array("status" => TRUE));
		}

		public function ajax_update() {
			$data = array(
					'smt' => $this->input->post('smt'),
					'soal' => $this->input->post('soal'),
					'jawaban' => $this->input->post('jawaban')
				);
			$this->crud->update(array('idsoal' => $this->input->post('idsoal')), $data);
			echo json_encode(array("status" => TRUE));
		}

		public function ajax_delete($id) {
			$this->crud->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}
	
	}
	
	/* End of file question.php */
	/* Location: ./application/controllers/admin/question.php */
?>