<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students_score extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "200ceb26807d6bf99fd6f4f0d1ca54d4") {
				redirect('login/admin','refresh');
			}
		$this->load->model('admin/student_score','score');
		$this->load->model('admin/Notification','notif');
		$this->countComplaint = $this->notif->notify()->num_rows();
	}

	public function index() {
			$data['jml'] = $this->countComplaint;
			$data['cabang'] = $this->score->getBranch();
			$this->load->view('admin/student_score',$data);
		}

		public function ajax_list($where) {
			$list = $this->score->get_datatables($where);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $scores) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $scores->nim;
				$row[] = $scores->Nama_Mahasiswa;
				$row[] = $scores->smt;
				$row[] = $scores->terjawab." / "."1000";
				$row[] = $scores->nilai." Points";
				// $row[] = $scores->jumlahjawab.' / '.$scores->jumlahsoal;
			
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->score->count_all($where),
							"recordsFiltered" => $this->score->count_filtered($where),
							"data" => $data,
					);
			//output to json format
			echo json_encode($output);
		}

}

/* End of file employers_score.php */
/* Location: ./application/controllers/admin/employers_score.php */