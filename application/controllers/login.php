<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Login extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->model('m_login');
			$this->load->model('supervisor/Model_super','data_campus');
			// $this->load->library('PHPExcel');
		}
		
		public function index() {
			$this->load->view('login');
		}

		public function DoLogin() {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$role = $this->input->post('role');
			if ($role == 'biodata') {
				$data = array(
							"nim" => $username,
							"tanggal_lahir" => $password
						);
			}
			elseif ($role == 'cabang') {
				$data = array(
							"username" => $username,
							"kodecabang" => $password
						);
			}
			else {
				$data = array(
							"nik" => $username,
							"tanggal" => $password
						);
			}
			$res = $this->m_login->login($role,$data)->num_rows();
			if ($res == 1) {
				if ($role == 'biodata') {
					$data_session = array('username' => $username,
										'status' => "5787be38ee03a9ae5360f54d9026465f");
					$this->session->set_userdata($data_session);
					redirect('student/dashboard','refresh');
				}
				elseif ($role == 'cabang') {
					$val = $this->data_campus->dataCampus($username)->result_array();
					foreach ($val as $key) {
						if ($key['kelompok'] == 0) {
							$table = "profilkaryawanpoltek";
							$kodecabang = $key['kodecabang'];
						} elseif($key['kelompok'] == 1) {
							$table = "profilkaryawancollege";
							$kodecabang = $key['kodecabang'];
						}
						
					}

					$data_session = array('username' => $username,
									  'status' => "09348c20a019be0318387c08df7a783d",
										'table' => $table,
										'code' => $kodecabang);
					$this->session->set_userdata($data_session);
					
					redirect('supervisor/dashboard','refresh');
				}
				elseif ($role == 'profilkaryawancollege') {
					$data_session = array('username' => $username,
									  'status' => "b0edd88cd46d6f0437f360bc0b992d5a");
					$this->session->set_userdata($data_session);
					
					redirect('college/dashboard','refresh');
				}
				elseif ($role == 'profilkaryawanpoltek') {
					$data_session = array('username' => $username,
									  'status' => "8f535d15b509f554428bc0e57c1f5cb3");
					$this->session->set_userdata($data_session);
					
					redirect('polytechnic/dashboard','refresh');
				}
				elseif ($role == 'profilkaryawanlcc') {
					$data_session = array('username' => $username,
									  'status' => "d18f6736f3e3f8a57f06409359a4cdd6");
					$this->session->set_userdata($data_session);
					
					redirect('lcc/dashboard','refresh');
				}
			}
			else{ ?>
				<script type="text/javascript"> 
					alert("Error! Current password or username is not correct, or maybe you are not registered.");
					window.location.href = "<?=base_url()?>";;
				</script>
<?php
				// redirect('login/index','refresh');
			}
		}

		public function admin() {
			$this->load->view('loginAdmin');
		}

		public function DoLoginAdmin() {
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$data = array("username" => $username,
						  "password" => $password);
			$res = $this->m_login->loginAdmin($data)->num_rows();
			if ($res == 1) {
				$data_session = array('username' => $username,
									  'status' => "200ceb26807d6bf99fd6f4f0d1ca54d4");
				$this->session->set_userdata($data_session);
				redirect('admin/dashboard','refresh');
			}
			else{ ?>
				<script type="text/javascript"> 
					alert("Error! Current password or username is not correct.");
					window.location.href = "<?=site_url('login/admin')?>";;
				</script>
<?php
				//redirect('login/admin','refresh');
			}
		}

		public function logout() {
			$this->session->sess_destroy();
			redirect(base_url(),'refresh');
		}

		public function NotFound() {
			$this->load->view('prank');
		}
	
	}
	
	/* End of file login.php */
	/* Location: ./application/controllers/login.php */
?>