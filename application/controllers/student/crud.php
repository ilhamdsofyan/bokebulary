<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') != '' || $this->session->userdata('status') == "5787be38ee03a9ae5360f54d9026465f") {
			$this->nim = $this->session->userdata('username');
		}
		else{
			redirect('login/NotFound','refresh');
		}
		$this->load->model('admin/student_model','crud');
		$this->load->library('user_agent');
		$this->load->helper('file');
	}

	public function update($nim) {
		// $where = array("nim" => $nim);
		$data = $this->input->post('Biodata');
		// print_r($where);
		
		$this->crud->updateExtra($nim,$data);
		redirect($this->agent->referrer(), 'refresh');
	}

	public function complaint($nim) {
		if (isset($_FILES['gambar']['name'])) {
			$filename = basename($_FILES['gambar']['name']);

			$config['upload_path'] = './assets/storage/attach/'; //buat folder dengan nama assets di root folder
			$config['file_name'] = $filename;
			$config['allowed_types'] = 'jpeg|jpg|png';
			$config['max_size'] = 128000;

			$this->load->library('upload');
			$this->upload->initialize($config);

			if(! $this->upload->do_upload('gambar') )
			$this->upload->display_errors();
		} else {
			$filename = "";
		}

		$data = array(
					"nim" => $nim,
					"judul" => $this->input->post('perihal'),
					"pesan" => $this->input->post('pesan'),
					"lampiran" => $filename,
					"tanggal" => date("Y-m-d h:i:s")
				);

		$this->db->insert('complain', $data);
		redirect($this->agent->referrer(), 'refresh');
	}

}

/* End of file crud.php */
/* Location: ./application/controllers/student/crud.php */