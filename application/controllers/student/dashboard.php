<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->model('student/dash','dash');
			$this->load->model('m_login');
			if ($this->session->userdata('username') != '' || $this->session->userdata('status') == "5787be38ee03a9ae5360f54d9026465f") {
				$this->nim = $this->session->userdata('username');
			}
			else{
				redirect('login/NotFound','refresh');
			}
			//get Profile
			$this->where = array("nim" => $this->nim);
			$this->data = $this->m_login->getProfile('biodata',$this->where);
		}
	
		public function index() {
			$data['biodata'] = $this->data;
			$this->load->view('student/hater/header', $data);
			$this->load->view('student/dashboard', $data);
			$this->load->view('student/hater/footer', $data);
		}
	
	}
	
	/* End of file dashboard.php */
	/* Location: ./application/controllers/admin/dashboard.php */
?>