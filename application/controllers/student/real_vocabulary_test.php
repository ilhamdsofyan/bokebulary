<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Real_vocabulary_test extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student/real_test','test');
		$this->load->model('m_login');

		if ($this->session->userdata('username') != '' || $this->session->userdata('status') != '5787be38ee03a9ae5360f54d9026465f') {
			$this->nim = $this->session->userdata('username');
			//get Profile
			$this->where = array("nim" => $this->nim);
			$this->data = $this->m_login->getProfile('biodata',$this->where);
			//Cek valid
			foreach ($this->data as $key) {
				if (date("Y") - $key->TahunAngkatan >= 2 && $key->tingkat == 3 || $key->status != "Aktif") {
					redirect('login/NotFound','refresh');
				}
			}
		}
		else{
			redirect('login/NotFound','refresh');
		}
	}

	/**
	 * @return [type]
	 */
	public function index() {
		$data['biodata'] = $this->data;
		$this->load->view('student/real_vtest',$data);
	}

	/**
	 * @param  [integer] $level
	 * @param  [integer] $been
	 * @return [type]
	 */
	public function test($level, $been = null) {
		if ($been != null) {
			$been = $been;
		}
		else{
			$been = 5400;
		}
		if ($level > 4 || $level < 1) {
			redirect('student/real_vocabulary_test/','refresh');
		}
		$array = array(
						"nim" => $this->nim,
						"smt" => $level
					);
		//get id test header
		$header = $this->test->getHeaderID($array);
		//Check if registered or not
		if (empty($header)) {
			$this->test->regHeader($level,$this->nim);
		}
		$header = $this->test->getHeaderID($array);
		$getid = $header[0]->idrealh;
		$idrealh = $getid;
		//Update New Lessons Time
		$been = $this->test->regTime($idrealh);
		// print_r($been);exit;
		if (!empty($header) && $been == null || $been == "") { ?>
			<script>
				alert("You've Been Followed The Real Test, Try To Go Another Level. Or If You Want Reset, Just Tell Your Lecture");
				window.location.href="<?=site_url('student/real_vocabulary_test/')?>";
			</script>
<?php	}
		$header = $this->test->getHeaderID($array);
		$getid = $header[0]->idrealh;
		//get Count Answered
		$idrealh = $getid;
		$idheader = array("idrealh" => $idrealh);

		//Check if finish
		$sum = $this->test->checkSum($idheader)->num_rows();
		if ($sum == 500 || $been <= 0) { 
			$this->test->upStatus($idheader);
			$this->test->removeTime($idheader);
?>
			<script>
				alert("You've Been Finished The Test");
				window.location.href="<?=site_url('student/real_vocabulary_test/real_score')?>";
			</script>";
		<?php }
		$data['answered'] = $this->test->countAnswered($idheader)->num_rows();
		//get Question
		$iseng = $this->test->getQuest($level,$idrealh);
		if (empty($iseng)) {
			$data['question'] = $this->test->getFirstQuest($level);
		}
		else{
			$data['question'] = $this->test->getQuest($level,$idrealh);
		}
		//get Multiple Choice
		foreach ($data['question'] as $key) {
			$idsoal[] = $key->idsoal;
		}
		//set Multiple Choice
		for ($i=0; $i < sizeof($idsoal); $i++) {
			$data[] = $this->test->getAnswer($idsoal[$i]);
		}
		//get Multiple Choice
		for ($i=0; $i < sizeof($idsoal); $i++) {
			$data['choice'][] = (array) $data[$i];
		}
		//get Profile
		$data['biodata'] = $this->data;
		//put String
		$data['level'] = "Vocabulary Lessons - Level ".$level;
		$data['idrealh'] = $idrealh;
		$data['been'] = $been;
		//load View
		$this->load->view('student/real_vocab_test', $data);
	}

	/**
	 * @param  [integer] $level
	 * @return [type]
	 */
	public function subnex($level) {
		$data['jawaban'] = $this->input->post('jawaban');
		$data['soal'] = $this->input->post('soal');
		$data['nim'] = $this->nim;
		$data['idrealh'] = $this->input->post('idrealh');
		$this->test->save_tesd($data);

		redirect("student/real_vocabulary_test/test/$level/",'refresh');
	}

}

/* End of file vocabulary_test.php */
/* Location: ./application/controllers/student/vocabulary_test.php */