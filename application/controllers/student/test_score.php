<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_score extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student/scores','score');
		$this->load->model('m_login');
		if ($this->session->userdata('username') != '' || $this->session->userdata('status') == "5787be38ee03a9ae5360f54d9026465f") {
			$this->nim = $this->session->userdata('username');
		}
		else{
			redirect('login/NotFound','refresh');
		}
		//get Profile
		$this->where = array("nim" => $this->nim);
		$this->data = $this->m_login->getProfile('biodata',$this->where);
	}

	public function index() {
		$data['biodata'] = $this->data;
		$data['headertest'] = $this->score->headTest($this->nim);
		$this->load->view('student/hater/header', $data);
		$this->load->view('student/score_header_test', $data);
		$this->load->view('student/hater/footer', $data);
	}

	public function detail($level) {
		$data['biodata'] = $this->data;
		$idtesh = $this->score->idheadTest($this->nim,$level);
		$data['detailtest'] = $this->score->detailTest($idtesh[0]->idtesh);
		$data['level'] = $level;
		$data['score'] = $this->score->getScoreTest($idtesh[0]->idtesh);
		$this->load->view('student/hater/header', $data);
		$this->load->view('student/score_detail_test', $data);
		$this->load->view('student/hater/footer', $data);
	}

}

/* End of file score.php */
/* Location: ./application/controllers/student/score.php */