<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vocabulary_test extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('student/test','test');
		$this->load->model('m_login');

		if ($this->session->userdata('username') != '' || $this->session->userdata('status') == "5787be38ee03a9ae5360f54d9026465f") {
			$this->nim = $this->session->userdata('username');
			//get Profile
			$this->where = array("nim" => $this->nim);
			$this->data = $this->m_login->getProfile('biodata',$this->where);
			//Cek valid
			foreach ($this->data as $key) {
				if (date("Y") - $key->TahunAngkatan >= 2 && $key->tingkat == 3 || $key->status != "Aktif") {
					redirect('login/NotFound','refresh');
				}
			}
		}
		else{
			redirect('login/NotFound','refresh');
		}
	}

	public function index() {
		$data['biodata'] = $this->data;
		$this->load->view('student/vtest',$data);
	}

	public function test($level) {
		if ($level > 4 || $level < 1) {
			redirect('student/vocabulary_test/','refresh');
		}
		$array = array(
						"nim" => $this->nim,
						"smt" => $level
					);
		//get id test header
		$header = $this->test->getHeaderID($array);
		//Check if registered or not
		if (empty($header)) {
			$this->test->regHeader($level,$this->nim);
		}
		else {
			//update date
			$this->test->upHeader($level,$this->nim);
			//Re-get
			$header = $this->test->getHeaderID($array);
			$getid = $header[0]->idtesh;
		}
		$header = $this->test->getHeaderID($array);
		$getid = $header[0]->idtesh;
		//get Count Answered
		$idtesh = $getid;
		$idheader = array("idtesh" => $idtesh);
		//Check if finish
		$sum = $this->test->checkSum($idheader)->num_rows();
		if ($sum == 1000) { 
			$this->test->upStatus($idheader);
?>
			<script>
				alert("You've Been Finished The Test!");
				window.location.href="<?=site_url('student/vocabulary_test/')?>";
			</script>";
		<?php }
		$data['answered'] = $this->test->countAnswered($idheader)->num_rows();
		//get Question
		$iseng = $this->test->getQuest($level,$idtesh);
		if (empty($iseng)) {
			$data['question'] = $this->test->getFirstQuest($level);
		}
		else{
			$data['question'] = $this->test->getQuest($level,$idtesh);
		}
		//get Multiple Choice
		foreach ($data['question'] as $key) {
			$idsoal[] = $key->idsoal;
		}
		//set Multiple Choice
		for ($i=0; $i < sizeof($idsoal); $i++) {
			$data[] = $this->test->getAnswer($idsoal[$i]);
		}
		//get Multiple Choice
		for ($i=0; $i < sizeof($idsoal); $i++) {
			$data['choice'][] = (array) $data[$i];
		}
		//get Profile
		$data['biodata'] = $this->data;
		//put String
		$data['level'] = "Vocabulary Test - Level ".$level;
		$data['idtesh'] = $idtesh;
		//load View
		$this->load->view('student/vocab_test', $data);
	}

	public function subnex($level) {
		$data['jawaban'] = $this->input->post('jawaban');
		$data['soal'] = $this->input->post('soal');
		$data['nim'] = $this->nim;
		$data['idtesh'] = $this->input->post('idtesh');
		$this->test->save_tesd($data);

		redirect("student/vocabulary_test/test/$level",'refresh');
	}

}

/* End of file vocabulary_test.php */
/* Location: ./application/controllers/student/vocabulary_test.php */