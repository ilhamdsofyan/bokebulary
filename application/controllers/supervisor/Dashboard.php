<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
			if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "09348c20a019be0318387c08df7a783d") {
				redirect('login/admin','refresh');
			}
			$this->load->model('supervisor/model_super', 'super');
			$this->user = $this->session->userdata('username');
		}
	
		public function index() {
			$data['anodite'] = $this->super->dataCampus($this->user)->result_array();
			$this->load->view('supervisor/dash', $data);
		}
	
	}
	
	/* End of file dashboard.php */
	/* Location: ./application/controllers/admin/dashboard.php */