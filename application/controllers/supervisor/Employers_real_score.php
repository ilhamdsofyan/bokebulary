<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employers_real_score extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "09348c20a019be0318387c08df7a783d") {
				redirect('login/admin','refresh');
			}

		$this->load->model('supervisor/model_super', 'super');
		$this->load->model('supervisor/model_employ_real', 'score');
		$this->user = $this->session->userdata('username');
	}

	public function index() {
		$data['anodite'] = $this->super->dataCampus($this->user)->result_array();
		$this->load->view('supervisor/real_employ', $data);
	}

	public function ajax_list() {
		$list = $this->score->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $scores) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $scores->nik;
			$row[] = $scores->nama;
			$row[] = $scores->smt;
			$row[] = $scores->terjawab." / 500";
			$row[] = $scores->nilai." Points";
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->score->count_all(),
						"recordsFiltered" => $this->score->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}

/* End of file Employers_score.php */
/* Location: ./application/controllers/supervisor/Employers_score.php */