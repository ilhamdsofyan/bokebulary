<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "09348c20a019be0318387c08df7a783d") {
			redirect('login','refresh');
		}
		$this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
		$this->load->model('supervisor/exporting', 'export');
	}

	public function employers() {
		ini_set("memory_limit", "500M");
		$kodecabang = $this->session->userdata('code');;
		$cabang = array("kodecabang" => $kodecabang);
		$table = $this->session->userdata('table');;

		$field = $this->export->selectField($table);
		$data = $this->export->selectData($table, $cabang);

		// echo sizeof($data);exit;
		if(sizeof($data) == 0){
			echo "<script>alert('Employers Data is Empty!');location.href='".redirect($this->agent->referrer(),'refresh')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Personal Data of Employers");

		$objSet = $phpExcel->setActiveSheetIndex(0);
		$objGet = $phpExcel->getActiveSheet();

		$objGet->setTitle("Bio Sheets"); //Sheet Title
		$objGet->getStyle("A1:AL1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);

		//Set Table Header
		$column = array(
						"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL"
					); //ActiveColumn
		//Field Name
		foreach ($field as $key) {
			$colValue[] = $key->name;
		}

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($data as $key) {
			$objSet->setCellValue("A".$start, $start-1);

			for ($i=1; $i < sizeof($column); $i++) { 
				$objSet->setCellValue($column[$i].$start, $key[$field[$i]->name]);
			}

			$phpExcel->getActiveSheet()->getStyle('AL1:AL'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		// $phpExcel->getActiveSheet()->setTitle("Data Exported");
		// $phpExcel->setActiveSheetIndex(0);

		$filename = urlencode("EmployersProfile_".$kodecabang."_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');

		redirect($this->agent->referrer(),'refresh');exit;
	}

	public function students() {
		ini_set("memory_limit", "500M");
		$kodecabang = $this->session->userdata('code');;
		$cabang = array("kodecabang" => $kodecabang);

		$field = $this->export->selectField("biodata");
		$data = $this->export->selectData("biodata", $cabang);
		// echo sizeof($data);exit;
		if(sizeof($data) == 0){
			echo "<script>alert('Students Data is Empty!');location.href='".site_url('admin/export_data')."'</script>";exit;
		}

		$phpExcel = new PHPExcel();
		//set properties
		$phpExcel->getProperties()
				 ->setCreator("Directorate of Central ICT")
				 ->setTitle("Personal Data of Students");

		$objSet = $phpExcel->setActiveSheetIndex(0);
		$objGet = $phpExcel->getActiveSheet();

		$objGet->setTitle("Bio Sheets"); //Sheet Title
		$objGet->getStyle("A1:Y1")->applyFromArray(
										array(
											"fill" => array(
														"type" => PHPExcel_Style_Fill::FILL_SOLID,
														"color" => array("rgb" => "2c3e50")
													),
											"font" => array(
														"color" => array("rgb" => "ecf0f1")
													)
										)
									);

		//Set Table Header
		$column = array(
						"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y"
					);//ActiveColumn
		//Field Name
		foreach ($field as $key) {
			$colValue[] = $key->name;
		}

		for ($i=0; $i < sizeof($column); $i++) { 
			$objSet->setCellValue($column[$i]."1", $colValue[$i]); //Fill Field Name into Column
		}

		//Fill Column with Data
		$start = 2; //Row Start at
		foreach ($data as $key) {
			$objSet->setCellValue("A".$start, $start-1);

			for ($i=1; $i < sizeof($column); $i++) { 
				$objSet->setCellValue($column[$i].$start, $key[$field[$i]->name]);
			}

			$phpExcel->getActiveSheet()->getStyle('Y1:Y'.$start)->getNumberFormat()->setFormatCode('0');

			$start++;
		}

		$filename = urlencode("StudentsProfile_".$kodecabang."_".date("d-m-Y").".xls");

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache

		ini_set("memory_limit", "16M");

		$objWriter = IOFactory::createWriter($phpExcel, 'Excel5');
		$objWriter->save('php://output');

		redirect($this->agent->referrer(),'refresh');exit;
	}

}

/* End of file Export.php */
/* Location: ./application/controllers/supervisor/Export.php */