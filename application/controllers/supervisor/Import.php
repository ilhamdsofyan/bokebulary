<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username') == '' || $this->session->userdata('status') != "09348c20a019be0318387c08df7a783d") {
			redirect('login','refresh');
		}
		$this->load->helper('file');
		$this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
	}

	public function employers() {
		ini_set("memory_limit", "500M");
		$fileName = basename($_FILES['fupload']['name']);

		$config['upload_path'] = './assets/storage/temp/'; //buat folder dengan nama assets di root folder
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 128000;

		$this->load->library('upload');
		$this->upload->initialize($config);

		if(! $this->upload->do_upload('fupload') )
		$this->upload->display_errors();

		$media = $this->upload->data('fupload');
		$inputFileName = './assets/storage/temp/'.$config['file_name'];

		try {
		$inputFileType = IOFactory::identify($inputFileName);
		$objReader = IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
			$rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
											NULL,
											TRUE,
											FALSE
										);

			//Field name initialize
			$data = array(
				"nik"					=> $rowData[0][0],
				"kodecabang"			=> $rowData[0][1],
				"nama"					=> $rowData[0][2],
				"tempat"				=> $rowData[0][3],
				"tanggal"				=> $rowData[0][4],
				"agama"					=> $rowData[0][5],
				"sex"					=> $rowData[0][6],
				"alamat"				=> $rowData[0][7],
				"email"					=> $rowData[0][8],
				"jenjangpend"			=> $rowData[0][9],
				"imagenya"				=> $rowData[0][10],
				"namapt"				=> $rowData[0][11],
				"mottohidup"			=> $rowData[0][12],
				"tglbergabung"			=> $rowData[0][13],
				"status"				=> $rowData[0][14],
				"jabatan"				=> $rowData[0][15],
				"bagian"				=> $rowData[0][16],
				"tglupdate"				=> $rowData[0][17],
				"statuskerja"			=> $rowData[0][18],
				"kelompok"				=> $rowData[0][19],
				"hp"					=> $rowData[0][20],
				"idskype"				=> $rowData[0][21],
				"facebook"				=> $rowData[0][22],
				"twitter"				=> $rowData[0][23],
				"noRekening"			=> $rowData[0][24],
				"NamaRekening"			=> $rowData[0][25],
				"jamsostek"				=> $rowData[0][26],
				"askes"					=> $rowData[0][27],
				"noKTP"					=> $rowData[0][28],
				"JenisSIM"				=> $rowData[0][29],
				"noSIM"					=> $rowData[0][30],
				"kewarganegaraan"		=> $rowData[0][30],
				"golDarah"				=> $rowData[0][31],
				"keluargadekat_nama"	=> $rowData[0][32],
				"keluargadekat_Alamat"	=> $rowData[0][33],
				"keluargadekat_telp"	=> $rowData[0][34],
				"statusperkawinan"		=> $rowData[0][35]
			);

			//Inserting Data
			$table = $this->session->userdata('table');;
			// print_r($table);exit;
			
			$insert = $this->db->insert("$table", $data) . ' ON DUPLICATE KEY UPDATE duplicate = LAST_INSERT_ID(duplicate)';;
			delete_files($config['upload_path']);
			ini_set("memory_limit", "16M");
		}
		redirect($this->agent->referrer(),'refresh');
	}

	public function students() {
		ini_set("memory_limit", "500M");
		$fileName = basename($_FILES['file']['name']);

		$config['upload_path'] = './assets/storage/temp/'; //buat folder dengan nama assets di root folder
		$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 128000;

		$this->load->library('upload');
		$this->upload->initialize($config);

		if(! $this->upload->do_upload('file') )
		$this->upload->display_errors();

		$media = $this->upload->data('file');
		$inputFileName = './assets/storage/temp/'.$config['file_name'];

		try {
			$inputFileType = IOFactory::identify($inputFileName);
			$objReader = IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
			$rowData = $sheet->rangeToArray('B' . $row . ':' . $highestColumn . $row,
			NULL,
			TRUE,
			FALSE);

			//Sesuaikan sama nama kolom tabel di database                                
			$data = array(
			"nim"				=> $rowData[0][0],
			"Nama_Mahasiswa"	=> $rowData[0][1],
			"tempat_lahir"		=> $rowData[0][2],
			"tanggal_lahir"		=> $rowData[0][3],
			"Sex"				=> $rowData[0][4],
			"Agama"				=> $rowData[0][5],
			"asal_smu"			=> $rowData[0][6],
			"thn_angkatan_smu"	=> $rowData[0][7],
			"jurusan_smu"		=> $rowData[0][8],
			"Alamat"			=> $rowData[0][9],
			"kodepos"			=> $rowData[0][10],
			"telepon"			=> $rowData[0][11],
			"kode"				=> $rowData[0][12],
			"tingkat"			=> $rowData[0][13],
			"kelas"				=> $rowData[0][14],
			"kodecabang"		=> $rowData[0][15],
			"NPM"				=> $rowData[0][16],
			"TahunAngkatan"		=> $rowData[0][17],
			"status"			=> $rowData[0][18],
			"NomorTranskrip"	=> $rowData[0][19],
			"jeniskelas"		=> $rowData[0][20],
			"foto"				=> $rowData[0][21],
			"ID"				=> $rowData[0][22],
			"email"				=> $rowData[0][23]
			);

			//Inserting Data
			$insert = $this->db->insert("biodata", $data);
			delete_files($config['upload_path']);
			ini_set("memory_limit", "16M");
		}
		redirect($this->agent->referrer(),'refresh');exit;
	}

}

/* End of file Import.php */
/* Location: ./application/controllers/supervisor/Import.php */