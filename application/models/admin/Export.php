<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Model {

	public function selectData($table) {
		$this->db->from($table);
		// $this->db->limit("1000");
		$data = $this->db->get();
		return $data->result_array();
	}

	public function selectPollege($table, $branch) {
		$this->db->from($table);
		$this->db->where($branch);

		$data = $this->db->get();
		return $data->result_array();
	}

	public function selectStudentData($branch) {
		$this->db->from('biodata');
		$this->db->where($branch);

		$data = $this->db->get();
		return $data->result_array();
	}

	public function selectField($table) {
		$result = $this->db->field_data($table);
		
		foreach ($result as $field) {
			$data[] = $field;
		}

		return $data;
	}

	public function selectBranch() {
		$this->db->select('kodecabang, namacabang');
		$this->db->from('cabang');
		$result = $this->db->get();

		return $result->result_array();
	}

	public function selectEmployTest($pilih, $where) {
		$this->db->select("$pilih.nik, $pilih.nama, karyawan_tesh.smt");
		$this->db->select('SUM(CASE WHEN karyawan_tesd.jawaban = karyawan_tesd.idsoal THEN 1 ELSE 0 END) AS nilai');
		$this->db->from($pilih);
		$this->db->join('karyawan_tesh', "karyawan_tesh.nik = $pilih.nik");
		$this->db->join('karyawan_tesd', "karyawan_tesd.nik = $pilih.nik");
		$this->db->join('karyawan_tesh a', 'a.idkarh = karyawan_tesd.idkarh');
		if(isset($where)){$this->db->where('kodecabang', $where);};
		$this->db->having("COUNT(karyawan_tesd.jawaban) > 0", "", FALSE);
	}

	public function selectEmployReal($pilih, $where) {
		$this->db->select("$pilih.nik, $pilih.nama, real_teskh.smt");
		$this->db->select('SUM(CASE WHEN real_teskd.jawaban = real_teskd.idsoal THEN 1 ELSE 0 END) AS nilai');
		$this->db->from($pilih);
		$this->db->join('real_teskh', "real_teskh.nik = $pilih.nik");
		$this->db->join('real_teskd', "real_teskd.nik = $pilih.nik");
		$this->db->join('real_teskh a', 'a.idrealkh = real_teskd.idrealkh');
		if(isset($where)){$this->db->where('kodecabang', $where);};
		$this->db->having("COUNT(real_teskd.jawaban) > 0", "", FALSE);
	}

	public function selectStudentTest($where) {
		$this->db->select('biodata.nim, biodata.Nama_Mahasiswa, tes_h.smt');
		$this->db->select('SUM(CASE WHEN tes_d.jawaban = tes_d.idsoal THEN 1 ELSE 0 END) AS nilai');
		$this->db->from("biodata");
		$this->db->join('tes_h', 'tes_h.nim = biodata.nim');
		$this->db->join('tes_d', 'tes_d.nim = biodata.nim');
		$this->db->join('tes_h a', 'a.idtesh = tes_d.idtesh');
		$this->db->where($where);
		$this->db->having("COUNT(tes_d.jawaban) > 0","",FALSE);

		$data = $this->db->get();
		return $data->result_array();
	}

	public function selectStudentReal($where) {
		$this->db->select('biodata.nim, biodata.Nama_Mahasiswa, real_tesh.smt');
		$this->db->select('SUM(CASE WHEN real_tesd.jawaban = real_tesd.idsoal THEN 1 ELSE 0 END) AS nilai');
		$this->db->from("biodata");
		$this->db->join('real_tesh', 'real_tesh.nim = biodata.nim');
		$this->db->join('real_tesd', 'real_tesd.nim = biodata.nim');
		$this->db->join('real_tesh a', 'a.idrealh = real_tesd.idrealh');
		$this->db->where($where);
		$this->db->having("COUNT(real_tesd.jawaban) > 0","",FALSE);

		$data = $this->db->get();
		return $data->result_array();
	}

}

/* End of file Export.php */
/* Location: ./application/models/admin/Export.php */