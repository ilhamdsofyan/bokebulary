<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manipulate extends CI_Model {

	public function update($key, $data) {
		$check = $this->db->get_where("user_login", $key)->num_rows();

		if ($check > 0) {
			$this->db->update('user_login', $data);
			$this->db->where($key);

			return 1;
		} else{
			return 0;
		}
	}	

}

/* End of file Manipulate.php */
/* Location: ./application/models/admin/Manipulate.php */