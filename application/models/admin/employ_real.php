<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Employ_real extends CI_Model {

		// var $table = 'biodata';
		var $column_order = array('nik','nama','smt','benar',null); //set column field database for datatable orderable
		var $column_search = array('nik','nama','smt','benar'); //set column field database for datatable searchable just firstname , lastname , address are searchable
		// var $order = array('nik' => 'desc'); // default order 

		private function _get_datatables_query($from,$where) {
			//Check what table want to show
			if ($from == "college") {
				$pilih = "profilkaryawancollege";
			}
			elseif ($from == "poltek") {
				$pilih = "profilkaryawanpoltek";
			}
			elseif ($from == "lcc") {
				$pilih = "profilkaryawanlcc";
			}
				$this->db->select("$pilih.nik, $pilih.nama, real_teskh.smt, COUNT(real_teskd.jawaban) AS terjawab");
				$this->db->select('SUM(CASE WHEN real_teskd.jawaban = real_teskd.idsoal THEN 1 ELSE 0 END) AS nilai');
				$this->db->from($pilih);
				$this->db->join('real_teskh', "real_teskh.nik = $pilih.nik");
				$this->db->join('real_teskd', "real_teskd.nik = $pilih.nik");
				$this->db->join('real_teskh a', 'a.idrealkh = real_teskd.idrealkh');
				if(isset($where)){$this->db->where('kodecabang', $where);};
				$this->db->having("COUNT(real_teskd.jawaban) > 0", "", FALSE);

			$i = 0;
		
			foreach ($this->column_search as $item) { // loop column 
				if($_POST['search']['value']) { // if datatable send POST for search
					
					if($i===0) { // first loop
						$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else {
						$this->db->or_like($item, $_POST['search']['value']);
					}

					if(count($this->column_search) - 1 == $i) //last loop
						$this->db->group_end(); //close bracket
				}
				$i++;
			}
			
			if(isset($_POST['order'])) { // here order processing
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order)) {
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function get_datatables($from,$where) {
			$this->_get_datatables_query($from,$where);
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered($from,$where) {
			$this->_get_datatables_query($from,$where);
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all($from, $where) {
			if ($from == "college") {
				$pilih = "profilkaryawancollege";
			}
			elseif ($from == "poltek") {
				$pilih = "profilkaryawanpoltek";
			}
			elseif ($from == "lcc") {
				$pilih = "profilkaryawanlcc";
			}

			$this->db->select("$pilih.nik, $pilih.nama, real_teskh.smt, COUNT(real_teskd.jawaban) AS terjawab");
			$this->db->select('SUM(CASE WHEN real_teskd.jawaban = real_teskd.idsoal THEN 1 ELSE 0 END) AS nilai');
			$this->db->from($pilih);
			$this->db->join('real_teskh', "real_teskh.nik = $pilih.nik");
			$this->db->join('real_teskd', "real_teskd.nik = $pilih.nik");
			$this->db->join('real_teskh a', 'a.idrealkh = real_teskd.idrealkh');
			if(isset($where)){$this->db->where('kodecabang', $where);};
			
			return $this->db->count_all_results();
		}

		public function getBranch() {
			$this->db->select('kodecabang, namacabang');
			$this->db->from('cabang');
			$query = $this->db->get();

			return $query->result();
		}

	}

	/* End of file employ_score.php */
	/* Location: ./application/models/admin/employ_score.php */