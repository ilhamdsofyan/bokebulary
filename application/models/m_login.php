<?php
	/**
	* 
	*/
	class M_login extends CI_model
	{
		
		function __construct()
		{
			parent::__construct();
		}

		public function login($table,$data) {
			if ($table != "biodata") {
				return $this->db->get_where($table,$data);exit;
			}

			$this->db->from($table);
			$this->db->where($data);
			$this->db->where('tingkat <', 3);
			$this->db->where('status', "Aktif");
			$data = $this->db->get();

			return $data;
		}

		public function loginAdmin($data) {
			return $this->db->get_where("user_login",$data);
		}

		public function getProfile($table,$data) {
			$this->db->from($table);
			if ($table != "profilkaryawanlcc") {
				$this->db->join("cabang", "cabang.kodecabang = ".$table.".kodecabang");
			}
			$this->db->where($data);
			$query = $this->db->get();
			return $query->result();
		}
		
	}
?>