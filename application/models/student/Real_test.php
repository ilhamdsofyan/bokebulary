<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Real_test extends CI_Model {

	/**
	 * @param  [array] $data
	 * @return [type]
	 */
	public function getHeaderID($data) {
		$this->db->from('real_tesh');
		$this->db->where($data);
		$data = $this->db->get();
		return $data->result();
	}

	/**
	 * @param  [array] $idrealh
	 * @return [type]
	 */
	public function countAnswered($idrealh) {
		return $this->db->get_where("real_tesd",$idrealh);
	}

	/**
	 * @param  [integer] $level
	 * @param  [integer] $idrealh
	 * @return [type]
	 */
	public function getQuest($level,$idrealh) {
		$ignore = "SELECT real_tesd.idsoal FROM real_tesd";
		$this->db->select('soal.idsoal, soal.soal');
		$this->db->from('soal,real_tesd,real_tesh');
		$this->db->where('soal.smt', $level);
		$this->db->where('real_tesd.idrealh', $idrealh);
		$this->db->where_not_in('soal.idsoal', $ignore);

		$this->db->order_by('rand()');
		$this->db->limit(5);

		$data = $this->db->get();
		return $data->result();
	}

	/**
	 * @param  [integer] $level
	 * @return [type]
	 */
	public function getFirstQuest($level) {
		$this->db->select('soal.idsoal, soal.soal');
		$this->db->from('soal');
		$this->db->where('soal.smt', $level);

		$this->db->order_by('rand()');
		$this->db->limit(5);

		$data = $this->db->get();
		return $data->result();
	}

	/**
	 * @param  [integer] $idsoal
	 * @return [type]
	 */
	public function getAnswer($idsoal) {
		$data = $this->db->query("SELECT * FROM( (SELECT idsoal, jawaban FROM soal ORDER BY RAND() LIMIT 3) UNION (SELECT idsoal, jawaban FROM soal WHERE idsoal = $idsoal ORDER BY RAND()) ) AS shit ORDER BY RAND()");
		return $data->result();
	}

	/**
	 * @param  [integer] $level
	 * @param  [string] $nim
	 * @return [type]
	 */
	public function regHeader($level,$nim) {
		$tanggal = date("Y/m/d");
		$data = array("nim" => $nim,
					"tanggaltes" => $tanggal,
					"smt" => $level,
					"status" => 0);
		$this->db->insert('real_tesh', $data);
	}

	/**
	 * @param  [integer] $level
	 * @param  [string] $nim
	 * @return [type]
	 */
	public function upHeader($level,$nim) {
		$tanggal = date("Y/m/d");
		$data = array("tanggaltes" => $tanggal);
		$where = array("smt" => $level,
					"nim" => $nim);
		$this->db->update('real_tesh', $data);
		$this->db->where($where);
	}

	/**
	 * @param  [array] $data
	 * @return [type]
	 */
	public function save_tesd($data) {
		for ($i=0; $i < sizeof($data['soal']); $i++) {
			$array = array("idrealh" => $data['idrealh'],
							"nim" => $data['nim'],
							"idsoal" => $data['soal'][$i],
							"jawaban" => $data['jawaban'][$i]
							);
			$this->db->insert('real_tesd', $array);
		}
	}

	/**
	 * @param  [array] $idrealh
	 * @return [type]
	 */
	public function checkSum($idrealh) {
		return $this->db->get_where("real_tesd",$idrealh);
	}

	/**
	 * @param  [array] $idrealh
	 * @return [type]
	 */
	public function upStatus($idrealh) {
		$object = array("status" => 1);
		$this->db->update('real_tesh', $object);
		$this->db->where($idrealh);
	}

	/**
	 * @param  integer $value
	 * @return [type]
	 */
	function checkTime($value=''){
		$this->db->from('timestamps');
		$this->db->where('idrealh', $value);
		$data = $this->db->get();
		return $data;
	}

	function regTime($idrealh){
		if ($this->checkTime($idrealh)->num_rows() == 0) {
			$object = array("idrealh" => $idrealh,
						"start" => date("h:i:s"),
						"end" => "00:00:00");
			$this->db->insert('timestamps', $object);
		}
		else{
			$object = array("end" => date("h:i:s"));
			$this->db->update('timestamps', $object);
			$this->db->where('idrealh', $idrealh);
		}
		$data = $this->checkTime($idrealh)->result_array();
		foreach ($data as $key) {
			if ($key['end'] != "00:00:00") {
				//Sec One
				$str_time1 = $key['start'];
				$str_time1 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time1);
				sscanf($str_time1, "%d:%d:%d", $hours, $minutes, $seconds);
				$time_seconds1 = $hours * 3600 + $minutes * 60 + $seconds;
				//Sec Two
				$str_time2 = $key['end'];
				$str_time2 = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time2);
				sscanf($str_time2, "%d:%d:%d", $hours, $minutes, $seconds);
				$time_seconds2 = $hours * 3600 + $minutes * 60 + $seconds;

				$decrease = $time_seconds2 - $time_seconds1;
				$time = 5400 - $decrease;
			}
			else{
				$time = 5400;
			}
		}
		return $time;
	}

	public function removeTime($id) {
		$this->db->delete('timestamps');
		$this->db->where($id);
	}
}

/* End of file test.php */
/* Location: ./application/models/student/test.php */