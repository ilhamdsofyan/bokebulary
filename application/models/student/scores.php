<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scores extends CI_Model {

	public function headTest($nim) {
		$this->db->from('tes_h');
		$this->db->where('nim', $nim);
		$this->db->order_by('tanggaltes', 'desc');
		$data = $this->db->get();
		return $data->result();
	}

	public function idheadTest($nim,$level) {
		$this->db->select('idtesh');
		$this->db->from('tes_h');
		$this->db->where('nim', $nim);
		$this->db->where('smt', $level);
		$data = $this->db->get();
		return $data->result();
	}

	public function headReal($nim) {
		$this->db->from('real_tesh');
		$this->db->where('nim', $nim);
		$data = $this->db->get();
		return $data->result();
	}

	public function detailTest($idtesh) {
		$data = $this->db->query("SELECT a.soal, (SELECT jawaban from soal where idsoal = b.idsoal) as jawabanuser, (select jawaban from soal where idsoal = c.idsoal) as jawabanbenar FROM soal a inner join tes_d b on a.idsoal = b.jawaban inner join soal c on a.idsoal = c.idsoal WHERE b.idtesh = $idtesh");
		return $data->result();
	}

	public function detailReal($nim,$level) {
		$this->db->from('real_tesd');
		$this->db->where('nim', $nim);
		$data = $this->db->get();
		return $data->result();
	}

	public function getScoreTest($idtesh) {
		$data = $this->db->query("SELECT COUNT(jawaban) AS score FROM `tes_d` WHERE idtesh = $idtesh AND idsoal = jawaban");
		return $data->result();
	}

}

/* End of file scores.php */
/* Location: ./application/models/student/scores.php */