<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Model {
	public function getHeaderID($data) {
		$this->db->from('tes_h');
		$this->db->where($data);
		$data = $this->db->get();
		return $data->result();
	}

	public function countAnswered($idtesh) {
		return $this->db->get_where("tes_d",$idtesh);
	}

	public function getQuest($level,$idtesh) {
		$ignore = "SELECT tes_d.idsoal FROM tes_d";
		$this->db->select('soal.idsoal, soal.soal');
		$this->db->from('soal,tes_d,tes_h');
		$this->db->where('soal.smt', $level);
		$this->db->where('tes_d.idtesh', $idtesh);
		$this->db->where_not_in('soal.idsoal', $ignore);

		$this->db->order_by('rand()');
		$this->db->limit(5);

		$data = $this->db->get();
		return $data->result();
	}

	public function getFirstQuest($level) {
		$data = $this->db->query("SELECT soal.idsoal, soal.soal FROM soal WHERE soal.smt = $level ORDER BY RAND() LIMIT 5");
		return $data->result();
	}

	public function getAnswer($idsoal) {
		$data = $this->db->query("SELECT * FROM( (SELECT idsoal, jawaban FROM soal ORDER BY RAND() LIMIT 3) UNION (SELECT idsoal, jawaban FROM soal WHERE idsoal = $idsoal ORDER BY RAND()) ) AS shit ORDER BY RAND()");
		return $data->result();
	}

	public function regHeader($level,$nim) {
		$tanggal = date("Y/m/d");
		$data = array("nim" => $nim,
					"tanggaltes" => $tanggal,
					"smt" => $level,
					"status" => 0);
		$this->db->insert('tes_h', $data);
	}

	public function upHeader($level,$nim) {
		$tanggal = date("Y/m/d");
		$data = array("tanggaltes" => $tanggal);
		$where = array("smt" => $level,
					"nim" => $nim);
		$this->db->update('tes_h', $data);
		$this->db->where($where);
	}

	public function save_tesd($data) {
		for ($i=0; $i < sizeof($data['soal']); $i++) {
			$array = array("idtesh" => $data['idtesh'],
							"nim" => $data['nim'],
							"idsoal" => $data['soal'][$i],
							"jawaban" => $data['jawaban'][$i]
							);
			$this->db->insert('tes_d', $array);
		}
	}

	public function checkSum($idtesh) {
		return $this->db->get_where("tes_d",$idtesh);
	}

	public function upStatus($idtesh) {
		$object = array("status" => 1);
		$this->db->update('tes_h', $object);
		$this->db->where($idtesh);
	}
}

/* End of file test.php */
/* Location: ./application/models/student/test.php */