<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_super extends CI_Model {	
	public function dataCampus($user) {
		$user = array("username" => $user);
		$data = $this->db->get_where("cabang", $user);
		return $data;
	}

	public function className($code) {
		$this->db->select('kelas');
		$this->db->from('biodata');
		$this->db->where('kodecabang', $code);
		$this->db->where('tingkat <= ', 2);
		$this->db->where('kelas <>', "");
		$this->db->group_by('kelas');

		$data = $this->db->get();
		return $data->result_array();
	}
}

/* End of file model_super.php */
/* Location: ./application/models/supervisor/model_super.php */