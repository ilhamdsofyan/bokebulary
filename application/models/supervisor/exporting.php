<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exporting extends CI_Model {

	public function selectField($table) {
		$result = $this->db->field_data($table);
		
		foreach ($result as $field) {
			$data[] = $field;
		}

		return $data;
	}

	public function selectData($table, $code) {
		$this->db->from($table);
		$this->db->where($code);

		$data = $this->db->get();
		return $data->result_array();
	}

}

/* End of file exporting.php */
/* Location: ./application/models/supervisor/exporting.php */