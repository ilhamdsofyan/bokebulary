<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Model_employ_real extends CI_Model {

		public function __construct() {
			parent::__construct();
			$this->table = $this->session->userdata("table");
			$this->code = $this->session->userdata("code");
			$this->column_order = array('nik', 'nama', 'smt', 'nilai', null); //set column field database for datatable orderable
			$this->column_search = array('nik', 'nama', 'smt', 'nilai'); //set column field database for datatable searchable just firstname , lastname , address are searchable
			// var $order = array('nik' => 'desc'); // default order 
		}

		private function _get_datatables_query() {
			$this->db->select("$this->table.nik, $this->table.nama, real_teskh.smt, COUNT(real_teskd.jawaban) AS terjawab");
			$this->db->select('SUM(CASE WHEN real_teskd.jawaban = real_teskd.idsoal THEN 1 ELSE 0 END) AS nilai');
			$this->db->from($this->table);
			$this->db->join('real_teskh', "real_teskh.nik = $this->table.nik");
			$this->db->join('real_teskd', "real_teskd.nik = $this->table.nik");
			$this->db->join('real_teskh a', 'a.idrealkh = real_teskd.idrealkh');
			$this->db->where('kodecabang', $this->code);
			$this->db->having("COUNT(real_teskd.jawaban) > 0", "", FALSE);

			$i = 0;
		
			foreach ($this->column_search as $item) { // loop column 
				if($_POST['search']['value']) { // if datatable send POST for search
					
					if($i===0) { // first loop
						$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, $_POST['search']['value']);
					}
					else {
						$this->db->or_like($item, $_POST['search']['value']);
					}

					if(count($this->column_search) - 1 == $i) //last loop
						$this->db->group_end(); //close bracket
				}
				$i++;
			}
			
			if(isset($_POST['order'])) { // here order processing
				$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			} 
			else if(isset($this->order)) {
				$order = $this->order;
				$this->db->order_by(key($order), $order[key($order)]);
			}
		}

		function get_datatables() {
			$this->_get_datatables_query();
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			return $query->result();
		}

		function count_filtered() {
			$this->_get_datatables_query();
			$query = $this->db->get();
			return $query->num_rows();
		}

		public function count_all() {
			$this->db->select("$this->table.nik, $this->table.nama, real_teskh.smt");
			$this->db->select('SUM(CASE WHEN tes_d.jawaban = tes_d.idsoal THEN 1 ELSE 0 END) AS benar');
			$this->db->select('SUM(CASE WHEN tes_d.jawaban <> tes_d.idsoal THEN 1 ELSE 0 END) AS salah');
			$this->db->select('COUNT(tes_d.jawaban) AS terjawab');
			$this->db->from($this->table);
			$this->db->join('real_teskh', "real_teskh.nik = $this->table.nik");
			$this->db->join('real_teskd', "real_teskd.nik = $this->table.nik");
			$this->db->join('real_teskh a', 'a.idrealkh = real_teskd.idrealkh');
			$this->db->where('kodecabang', $this->code);

			return $this->db->count_all_results();
		}

		// public function getBranch() {
		// 	$this->db->select('kodecabang, namacabang');
		// 	$this->db->from('cabang');
		// 	$query = $this->db->get();

		// 	return $query->result();
		// }

	}

	/* End of file employ_score.php */
	/* Location: ./application/models/admin/employ_score.php */