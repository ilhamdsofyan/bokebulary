<?php $this->load->view('admin/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
  a{
    cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>

        <div class="content-panel">
          <div class="col-xs-8">
            <h4><i class="fa fa-angle-right"></i> College Employers List</h4>
          </div>
          <div class="col-xs-4" align="right">
            <button class="btn btn-success btn-md" onclick="add_college()"><i class="fa fa-plus"></i>&emsp; Add</button>
          </div>
          <section id="unseen">
            <br><br><br><br>
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Branch Name</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>D o B / Password</th>
                  <th width="150px">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Branch Name</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>D o B / Password</th>
                  <th width="125px">Action</th>
                </tr>
              </tfoot>
            </table>
          </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
        
<?php $this->load->view('admin/hater/footer.php'); ?>
<script>
$(document).ready(function() {
  //datatables
  table = $('#example').DataTable({ 

      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo site_url('admin/employers_college/ajax_list')?>",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
      },
      ],

  });

  //DatePicker
$('.datepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true 
});
})

  function add_college() {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Add Colleges'); // Set Title to Bootstrap modal title
  }

  function edit_college(id) {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('admin/employers_college/ajax_edit/')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data) {
            $('[name="id_col"]').val(data.id_col);
            $('[name="nik"]').val(data.nik);
            $('[name="nama"]').val(data.nama);
            $('[name="kodecabang"]').val(data.kodecabang).prop('selected',true);
            $('[name="alamat"]').val(data.alamat);
            $('[name="tempat"]').val(data.tempat);
            $('[name="tanggal"]').val(data.tanggal);
            $('[name="email"]').val(data.email);
            $('[name="tglbergabung"]').val(data.tglbergabung);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Colleges'); // Set title to Bootstrap modal title

          },
          error: function (jqXHR, textStatus, errorThrown) {
              alert('Error get data from ajax');
          }
      });
  }

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('admin/employers_college/ajax_add')?>";
      } else {
          url = "<?php echo site_url('admin/employers_college/ajax_update')?>";
      }

      // ajax adding data to database
      $.ajax({
          url : url,
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  reload_table();
              }

              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 

          }
      });
  }

  function delete_college(id) {
      if(confirm('Are you sure delete this data?'))
      {
          // ajax delete data to database
          $.ajax({
              url : "<?php echo site_url('admin/employers_college/ajax_delete')?>/"+id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  $('#modal_form').modal('hide');
                  reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });

      }
  }
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Colleges Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_col"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="nik">NIK</label>
                            <div class="col-md-9">
                                <input type="text" name="nik" id="nik" placeholder="NIK" class="form-control">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="nama">Name</label>
                            <div class="col-md-9">
                                <input type="text" name="nama" id="nama" placeholder="Full Name" class="form-control">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="kodecabang">Branch Name</label>
                            <div class="col-md-9">
                                <select name="kodecabang" id="kodecabang" class="form-control">
                                  <option value=""> - Choose Branch Name - </option>
                                  <?php foreach ($cabang as $key) { ?>
                                    <option value="<?=$key->kodecabang?>"> <?=$key->namacabang?> </option>
                                  <?php } ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="alamat">Address</label>
                            <div class="col-md-9">
                                <textarea name="alamat" id="alamat" placeholder="Address" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="tempat">P o B</label>
                            <div class="col-md-9">
                                <input type="text" name="tempat" id="tempat" placeholder="Place of Birth" class="form-control">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="tanggal">D o B</label>
                            <div class="col-md-9">
                                <input name="tanggal" id="tanggal" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="email">Email</label>
                            <div class="col-md-9">
                                <input name="email" id="email" placeholder="example@xx.com" class="form-control" type="email">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="tanggal">Join Date</label>
                            <div class="col-md-9">
                                <input name="tglbergabung" id="tglbergabung" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->