<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
	a {
		cursor: pointer !important;
	}
	/*img {
		transition: all .2s ease-in-out;
	}
	img:hover {
		transform: scale(2);
		cursor: -webkit-zoom-in; cursor: -moz-zoom-in;
	}*/
</style>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-xs-9" id="isi">
				<h3>Welcome To Our Vocabulary Online</h3>
				<br>
				<div class="content-panel">
					<div class="col-xs-8">
						<h4><i class="fa fa-angle-right"></i> Complaint Messages</h4>
					</div>
					<section id="unseen">
						<br><br><br><br>
							<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Purposes</th>
										<th width="150px">Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Purposes</th>
										<th width="150px">Action</th>
									</tr>
								</tfoot>
							</table>
					</section>
				</div>
			</div><!-- /col-xs-9 END SECTION MIDDLE -->
				
<?php $this->load->view('admin/hater/footer.php'); ?>
<script>

$(document).ready(function() {
var table = $("#example").DataTable({"destroy" : true});
	table = $("#example").DataTable({ 
		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"destroy" : true, //Destroying old table
		"order": [], //Initial no order.

		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": "<?=site_url()?>admin/Complaint_page/ajax_list/",
			"type": "POST"
		},

		//Set column definition initialisation properties.
		"columnDefs": [ { 
				"targets": [ -1 ], //last column
				"orderable": false, //set not orderable
			},
		],
	});
})

function reload_table() {
	$("#example").DataTable().ajax.reload(null,false); //reload datatable ajax 
}

function show(id) {
	$("#modal_form").removeData();
	// $('#form')[0].reset(); // reset form on modals
	$('.form-group').removeClass('has-error'); // clear error class
	$('.help-block').empty(); // clear error string

	//Ajax Load data from ajax
	$.ajax({
		url : "<?php echo site_url('admin/Complaint_page/ajax_show/')?>/" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data) {
			$('[name="id"]').val(data.id);
			$('[name="nim"]').val(data.nim);
			$('[name="Nama_Mahasiswa"]').val(data.Nama_Mahasiswa);
			$('[name="namacabang"]').val(data.namacabang);
			$('[name="judul"]').val(data.judul);
			$('[name="pesan"]').val(data.pesan);

			if (data.lampiran != "" || data.lampiran != "null" || data.lampiran != null) {
				$("#lampiran").attr("src", "<?=base_url('assets/storage/attach/')?>"+data.lampiran);
			}
			if (data.lampiran == "" || data.lampiran == "null" || data.lampiran == null) {
				$("#lampiran").attr("src", "<?=base_url('assets/img/none.jpg')?>");
			}

			$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
			$('.modal-title').text('Message Detail'); // Set title to Bootstrap modal title

		},
		error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
		}
	});
}

function delete_complaint(id) {
	if(confirm('Are you sure delete this data?')) {
		// ajax delete data to database
		$.ajax({
			url : "<?php echo site_url('admin/complaint_page/ajax_delete')?>/"+id,
			type: "POST",
			dataType: "JSON",
			success: function(data)
			{
				//if success reload ajax table
				$('#modal_form').modal('hide');
				reload_table();
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error deleting data');
			}
		});
	}
  }
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Complaint Message</h3>
			</div>
			<div class="modal-body form">
				<div class="form-group">
					<label class="control-label col-md-3">Unique ID</label>
					<div class="col-md-9">
						<input type="text" name="nim" id="dari" class="form-control" readonly>
					</div>
				</div>
				<br><br>
				<div class="form-group">
					<label class="control-label col-md-3">From</label>
					<div class="col-md-9">
						<input type="text" name="Nama_Mahasiswa" id="dari" class="form-control" readonly>
					</div>
				</div>
				<br><br>
				<div class="form-group">
					<label class="control-label col-md-3">Branch</label>
					<div class="col-md-9">
						<input type="text" name="namacabang" id="cabang" class="form-control" readonly>
					</div>
				</div>
				<br><br>
				<div class="form-group">
					<label class="control-label col-md-3">Purpose</label>
					<div class="col-md-9">
						<input type="text" name="judul" id="judul" class="form-control" readonly>
					</div>
				</div>
				<br><br>
				<div class="form-group">
					<div class="col-md-12">
						<textarea name="pesan" id="pesan" class="form-control" readonly></textarea>
					</div>
					<div class="col-md-12"><hr><hr></div>
				</div>
				<div class="row">
					<div class="col-md-12" align="center">
						<img src="" style="width: 50%;" id="lampiran">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->