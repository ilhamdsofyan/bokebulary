<?php $this->load->view('admin/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> -->
<style type="text/css">
  a{
	cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-xs-9" id="isi">
				<h3>Welcome To Our Vocabulary Online</h3>
				<br>
				<div class="content-panel">
					<div class="row">
						<div class="col-xs-9">
							<h4><i class="fa fa-angle-right"></i> Employer's Polytechnic Data</h4>
						</div>
						<div class="col-xs-1">
							<button class="btn btn-default" id="poltek">Export / Import</button>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<h4><i class="fa fa-angle-right"></i> Employer's College Data</h4>
						</div>
						<div class="col-xs-1">
							<button class="btn btn-primary" id='college'>Export / Import</button>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<h4><i class="fa fa-angle-right"></i> Employer's LCC Data</h4>
						</div>
						<div class="col-xs-1">
							<button class="btn btn-warning" id='lcc'>Export / Import</button>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<h4><i class="fa fa-angle-right"></i> Student's Data</h4>
						</div>
						<div class="col-xs-1">
							<button class="btn btn-success" id='student'>Export / Import</button>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<h4><i class="fa fa-angle-right"></i> Scores</h4>
						</div>
						<div class="col-xs-1">
							<button class="btn btn-danger" id='score')>Download</button>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
				</div>
			</div><!-- /col-xs-9 END SECTION MIDDLE -->
		
<?php $this->load->view('admin/hater/footer.php'); ?>

<!-- F*cking Modal -->
<script type="text/javascript">
	$("#poltek").click(function(){
		$("#modal_form").removeData();
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export / Import Data Polytechnic'); // Set Title to Bootstrap modal title
		$("#type").val("profilkaryawanpoltek");
		$("#tipe").val("profilkaryawanpoltek");
		$("#format_file").prop("href", "<?=base_url('assets/storage/table_format/karyawan_table_format.xlsx')?>");
		$("#export_excel").prop("href", "<?=site_url('admin/export_data/pollege_employs/profilkaryawanpoltek')?>");
		$("#form2").prop("action", "<?=site_url('admin/import_data/pollege_employs')?>");
		$("#form1").prop("action", "<?=site_url('admin/export_data/pollege_employs')?>");
		$("#cabang").show();
		$("#text").hide();
	})
	$("#college").click(function(){
		$("#modal_form").removeData();
		$('#form')[0].reset(); // reset form on modals
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export / Import Data College'); // Set Title to Bootstrap modal title
		$("#type").val("profilkaryawancollege");
		$("#tipe").val("profilkaryawancollege");
		$("#format_file").prop("href", "<?=base_url('assets/storage/table_format/karyawan_table_format.xlsx')?>");
		$("#export_excel").prop("href", "<?=site_url('admin/export_data/pollege_employs/profilkaryawancollege')?>");
		$("#form2").prop("action", "<?=site_url('admin/import_data/pollege_employs')?>");
		$("#form1").prop("action", "<?=site_url('admin/export_data/pollege_employs')?>");
		$("#cabang").show();
		$("#text").hide();
	})
	$("#lcc").click(function() {
		$("#modal_form").removeData();
		$('#form')[0].reset(); // reset form on modals
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export / Import Data LCC'); // Set Title to Bootstrap modal title
		$("#type").val("profilkaryawanlcc");
		$("#tipe").val("profilkaryawanlcc");
		$("#format_file").prop("href", "<?=base_url('assets/storage/table_format/lcc_table_format.xlsx')?>");
		$("#export_excel").prop("href", "<?=site_url('admin/export_data/exportToExcel/profilkaryawanlcc')?>");
		$("#form2").prop("action", "<?=site_url('admin/import_data/lcc_employs')?>");
		$("#form1").prop("action", "<?=site_url('admin/export_data/lcc_employs')?>");
		$("#cabang").hide();
		$("#text").show();
	})
	$("#student").click(function(){
		$("#modal_student").removeData();
		$('#form')[0].reset(); // reset form on modals
		$('#modal_student').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export / Import Data Student'); // Set Title to Bootstrap modal title
		$("#format_file").prop("href", "<?=base_url('assets/storage/table_format/mahasiswa_table_format.xlsx')?>");
	})
	$("#score").click(function(){
		$("#modal_score").removeData();
		$('#form')[0].reset(); // reset form on modals
		$('#modal_score').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export Scores'); // Set Title to Bootstrap modal title
	})
</script>

<!-- Bootstrap modal All -->
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<div class="modal-body form">
				<div class="form-body">
					<div class="form-group" align="left">
						<div class="col-md-6">
							<label>Don't have the Excel format ?</label>
						</div>
						<div class="col-md-6">
							<a href="#" class="btn btn-warning" id="format_file"><i class="fa fa-file"></i>&nbsp;Click Here !</a>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<form name="form1" id="form1" class="form-horizontal" method="post" >
						<div class="form-group" align="left">
							<input type="hidden" name="type" id="type">
							<div class="col-md-6">
								<select class="form-control" name="cabang" id="cabang">
									<option disabled selected> - Choose Branch - </option>
									<?php foreach ($cabang as $key) { ?>
										<option value="<?=$key['kodecabang']?>"> <?=$key['namacabang']?> </option>
									<?php } ?>
								</select>
								<label id="text">Import LCC Employers Data</label>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-danger">Export File</button>
							</div>
							<div class="col-lg-12">
								<hr>
							</div>
						</div>
					</form>
					<form id="form2" class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" name="tipe" id="tipe" />
						<div class="form-group">
							<label class="control-label col-md-3" for="fupload">File to Import</label>
							<div class="col-md-6">
								<input type="file" name="fupload" id="fupload" class="form-control" accept="application/vnd.ms-excel">
								<span class="help-block">* Only Accept .XLS / Excel File.</span>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-upload"></i>&nbsp;Import File
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal Student -->
<div class="modal fade" id="modal_student" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<div class="modal-body form">
				<div class="form-body">
					<div class="form-group" align="left">
						<div class="col-md-6">
							<label>Don't have the Excel format ?</label>
						</div>
						<div class="col-md-6">
							<a href="#" class="btn btn-warning" id="format_file"><i class="fa fa-file"></i>&nbsp;Click Here !</a>
						</div>
						<div class="col-lg-12">
							<hr>
						</div>
					</div>
					<form action="<?=site_url('admin/export_data/students')?>" name="form1" class="form-horizontal" method="post">
						<div class="form-group" align="left">
							<div class="col-md-6">
								<select class="form-control" name="cabang">
									<option disabled selected> - Choose Branch - </option>
									<?php foreach ($cabang as $key) { ?>
										<option value="<?=$key['kodecabang']?>"> <?=$key['namacabang']?> </option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-danger">Export File</button>
							</div>
							<div class="col-lg-12">
								<hr>
							</div>
						</div>
					</form>
					<form action="<?=site_url('admin/import_data/students')?>" name="form2" id="form" class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" name="type" id="type" />
						<div class="form-group">
							<label class="control-label col-md-3" for="file">File to Import</label>
							<div class="col-md-6">
								<input type="file" name="file" id="file" class="form-control" accept="application/vnd.ms-excel">
								<span class="help-block">* Only Accept .XLS / Excel File.</span>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-upload"></i>&nbsp;Import File
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal Scores -->
<div class="modal fade" id="modal_score" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<div class="modal-body form">
				<div class="form-body">
					<form action="<?=site_url('admin/export_data/export_employ_score')?>" name="form1" class="form-horizontal" method="post">
						<div class="form-group" align="left">
							<div class="col-lg-12" align="center">
								<label style="font-weight: bold;">Employers Score</label>
							</div>
							<div class="col-md-4">
								<select class="form-control" name="tipe">
									<option disabled selected> - LP3I Group - </option>
									<option value="profilkaryawancollege"> College </option>
									<option value="profilkaryawanpoltek"> Polytechnic </option>
									<option value="profilkaryawanlcc"> LCC </option>
								</select>
							</div>
							<div class="col-md-5">
								<select class="form-control" name="cabang">
									<option disabled selected> - Choose Branch - </option>
									<?php foreach ($cabang as $key) { ?>
										<option value="<?=$key['kodecabang']?>"> <?=$key['namacabang']?> </option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-primary">Download</button>
							</div>
							<div class="col-lg-12">
								<hr>
							</div>
						</div>
					</form>
					<form action="<?=site_url('admin/export_data/export_student_score')?>" name="form1" class="form-horizontal" method="post">
						<div class="form-group" align="center">
							<div class="col-lg-12">
								<label style="font-weight: bold;">Students Score</label>
							</div>
							<div class="col-md-9">
								<select class="form-control" name="cabang">
									<option disabled selected> - Choose Branch - </option>
									<?php foreach ($cabang as $key) { ?>
										<option value="<?=$key['kodecabang']?>"> <?=$key['namacabang']?> </option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-success">Download</button>
							</div>
							<div class="col-lg-12">
								<hr>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->