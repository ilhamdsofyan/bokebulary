<?php $this->load->view('admin/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> -->
<style type="text/css">
  a{
    cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        
        <div class="content-panel">
          <div class="col-xs-8">
            <h4><i class="fa fa-angle-right"></i> Questions List</h4>
          </div>
          <div class="col-xs-4" align="right">
            <button class="btn btn-success btn-md" onclick="add_quest()"><i class="fa fa-plus"></i>&emsp; Add</button>
          </div>
          <section id="unseen">
            <br><br><br><br>
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Level</th>
                  <th>Questions</th>
                  <th>Answer</th>
                  <th width="150px">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Level</th>
                  <th>Questions</th>
                  <th>Answer</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
        
<?php $this->load->view('admin/hater/footer.php'); ?>
<script>
$(document).ready(function() {
  //datatables
  table = $('#example').DataTable({ 

      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "order": [], //Initial no order.

      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "<?php echo site_url('admin/questions/ajax_list')?>",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
      },
      ],

  });
})

  function add_quest() {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Add Questions'); // Set Title to Bootstrap modal title
  }

  function edit_quest(id) {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
          url : "<?php echo site_url('admin/questions/ajax_edit/')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data) {
            $('[name="idsoal"]').val(data.idsoal);
            $('[name="smt"]').val(data.smt);
            $('[name="soal"]').val(data.soal);
            $('[name="jawaban"]').val(data.jawaban);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Questions'); // Set title to Bootstrap modal title

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function save()
  {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      var url;

      if(save_method == 'add') {
          url = "<?php echo site_url('admin/questions/ajax_add')?>";
      } else {
          url = "<?php echo site_url('admin/questions/ajax_update')?>";
      }

      // ajax adding data to database
      $.ajax({
          url : url,
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {
                  $('#modal_form').modal('hide');
                  reload_table();
              }

              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 

          }
      });
  }

  function delete_quest(id) {
      if(confirm('Are you sure delete this data?'))
      {
          // ajax delete data to database
          $.ajax({
              url : "<?php echo site_url('admin/questions/ajax_delete')?>/"+id,
              type: "POST",
              dataType: "JSON",
              success: function(data)
              {
                  //if success reload ajax table
                  $('#modal_form').modal('hide');
                  reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  alert('Error deleting data');
              }
          });

      }
  }
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Questions Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="idsoal"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="smt">Level</label>
                            <div class="col-md-9">
                                <select name="smt" id="smt" class="form-control">
                                  <option value=""> - Select Level - </option>
                                  <?php for ($i=1; $i < 5; $i++) { ?>
                                    <option value="<?=$i?>"> Level <?=$i?> </option>
                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="soal">Questions</label>
                            <div class="col-md-9">
                                <input name="soal" id="soal" placeholder="Questions" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="jawaban">Answer</label>
                            <div class="col-md-9">
                                <input name="jawaban" id="jawaban" placeholder="Answer" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->