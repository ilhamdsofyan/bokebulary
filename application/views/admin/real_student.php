<?php $this->load->view('admin/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
  a{
    cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        <div class="content-panel">
            
              <label for="branch">Select Campus</label>
                <select name="branch" id="branch" class="form-control">
                  <option disabled selected> - Choose Branch Name - </option>
                  <?php foreach ($cabang as $key) { ?>
                    <option value="<?=$key->kodecabang?>"> <?=$key->namacabang?> </option>
                  <?php } ?>
                </select>
            <br>

            <button class="btn btn-default" style="margin-left: 20px;" id="show"><i class="fa fa-search"></i>&nbsp;Search</button>
        </div>
        <br>
        <div class="content-panel">
          <div class="col-xs-8">
            <h4><i class="fa fa-angle-right"></i> Students Real Score</h4>
          </div>
          <section id="unseen">
            <br><br><br><br>
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Name</th>
                  <th>Level</th>
                  <th>Answered</th>
                  <th>Score</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Name</th>
                  <th>Level</th>
                  <th>Answered</th>
                  <th>Score</th>
                </tr>
              </tfoot>
            </table>
          </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
        
<?php $this->load->view('admin/hater/footer.php'); ?>
<script>
  $(document).ready(function() {
    var table = $("#example").DataTable({"destroy" : true});
    $('#show').click(function() {

      var cabang = $("#branch").val();
      // $("kodecabang").val = cabang;
      if (cabang==null) {
        return alert("Please select the campus!");
      }
      //datatables
      table = $("#example").DataTable({ 

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "destroy" : true, //Destroying old table
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?=site_url()?>admin/students_real_score/ajax_list/"+cabang,
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],
      });
  })

//DatePicker
  $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      orientation: "top auto",
      todayBtn: true,
      todayHighlight: true 
  });

//YearPicker
  $('.yearpicker').datepicker({
      autoclose: true,
      format: "yyyy",
      orientation: "top auto",
      todayBtn: true,
      todayHighlight: true 
  });

})
</script>