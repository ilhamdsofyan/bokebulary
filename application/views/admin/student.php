<?php $this->load->view('admin/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
  a{
	cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-xs-9" id="isi">
				<h3>Welcome To Our Vocabulary Online</h3>
				<br>
					<div class="content-panel">
						<label for="branch">Select Campus</label>
						<select name="branch" id="branch" class="form-control">
							<option disabled selected> - Choose Branch Name - </option>
							<?php foreach ($cabang as $key) { ?>
								<option value="<?=$key->kodecabang?>"> <?=$key->namacabang?> </option>
							<?php } ?>
						</select>
						<br>
						<button class="btn btn-default" style="margin-left: 20px;" id="show"><i class="fa fa-search"></i>&nbsp;Search</button>
					</div>
					<br>
			<div class="content-panel">
				<div class="col-xs-8">
					<h4><i class="fa fa-angle-right"></i> Students List</h4>
				</div>
				<div class="col-xs-4" align="right">
					<button id="adders" class="btn btn-success btn-md" onclick="add_college()"><i class="fa fa-plus"></i>&emsp; Add</button>
				</div>
				<section id="unseen">
					<br><br><br><br>
					<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Branch Name</th>
								<th>NIM</th>
								<th>Name</th>
								<th>D o B / Password</th>
								<th width="150px">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Branch Name</th>
								<th>NIM</th>
								<th>Name</th>
								<th>D o B / Password</th>
								<th width="120px">Action</th>
							</tr>
						</tfoot>
					</table>
				</section>
			</div>
	  </div><!-- /col-xs-9 END SECTION MIDDLE -->
		
<?php $this->load->view('admin/hater/footer.php'); ?>
<script>

	$(document).ready(function() {
	var table = $("#example").DataTable({"destroy" : true});
	$("#adders").hide();
	$('#show').click(function() {
	var cabang = $("#branch").val();
	// $("kodecabang").val = cabang;
	if (cabang==null) {
	  return alert("Please select the campus!");
	}
	$("#adders").show();
	//datatables
	table = $("#example").DataTable({ 

		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"destroy" : true, //Destroying old table
		"order": [], //Initial no order.

		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": "<?=site_url()?>admin/students/ajax_list/"+cabang,
			"type": "POST"
		},

		//Set column definition initialisation properties.
		"columnDefs": [
		{ 
			"targets": [ -1 ], //last column
			"orderable": false, //set not orderable
		},
		],

	});
	})

	//DatePicker
	$('.datepicker').datepicker({
	autoclose: true,
	format: "yyyy-mm-dd",
	orientation: "top auto",
	todayBtn: true,
	todayHighlight: true 
	});

	//YearPicker
	$('.yearpicker').datepicker({
	autoclose: true,
	format: "yyyy",
	orientation: "top auto",
	todayBtn: true,
	todayHighlight: true 
	});

	})

	function add_college() {
	  save_method = 'add';
	  $('#form')[0].reset(); // reset form on modals
	  $('.form-group').removeClass('has-error'); // clear error class
	  $('.help-block').empty(); // clear error string
	  $('#modal_form').modal('show'); // show bootstrap modal
	  $('.modal-title').text('Add Students'); // Set Title to Bootstrap modal title
	}

	function edit_college(id) {
	  save_method = 'update';
	  $('#form')[0].reset(); // reset form on modals
	  $('.form-group').removeClass('has-error'); // clear error class
	  $('.help-block').empty(); // clear error string

	  //Ajax Load data from ajax
	  $.ajax({
		  url : "<?php echo site_url('admin/students/ajax_edit/')?>/" + id,
		  type: "GET",
		  dataType: "JSON",
		  success: function(data) {
			$('[name="id_sis"]').val(data.id_sis);
			$('[name="nim"]').val(data.nim);
			$('[name="Nama_Mahasiswa"]').val(data.Nama_Mahasiswa);
			$('[name="kodecabang"]').val(data.kodecabang).prop('selected',true);
			$('[name="Alamat"]').val(data.Alamat);
			$('[name="tempat_lahir"]').val(data.tempat_lahir);
			$('[name="tanggal_lahir"]').val(data.tanggal_lahir);
			$('[name="telepon"]').val(data.telepon);
			$('[name="TahunAngkatan"]').val(data.TahunAngkatan);
			$('[name="kelas"]').val(data.kelas);
			$('[name="tingkat"]').val(data.tingkat);
			$('[name="status"]').val(data.status);
			$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
			$('.modal-title').text('Edit Students'); // Set title to Bootstrap modal title

		  },
		  error: function (jqXHR, textStatus, errorThrown) {
			  alert('Error get data from ajax');
		  }
	  });
	}

	function reload_table()
	{
	$("#example").DataTable().ajax.reload(null,false); //reload datatable ajax 
	}

	function save() {
	  $('#btnSave').text('saving...'); //change button text
	  $('#btnSave').attr('disabled',true); //set button disable 
	  var url;

	  if(save_method == 'add') {
		  url = "<?php echo site_url('admin/students/ajax_add')?>";
	  } else {
		  url = "<?php echo site_url('admin/students/ajax_update')?>";
	  }

	  // ajax adding data to database
	  $.ajax({
		  url : url,
		  type: "POST",
		  data: $('#form').serialize(),
		  dataType: "JSON",
		  success: function(data)
		  {

			  if(data.status) //if success close modal and reload ajax table
			  {
				  $('#modal_form').modal('hide');
				  reload_table();
			  }

			  $('#btnSave').text('save'); //change button text
			  $('#btnSave').attr('disabled',false); //set button enable 


		  },
		  error: function (jqXHR, textStatus, errorThrown)
		  {
			  alert('Error adding / update data');
			  $('#btnSave').text('save'); //change button text
			  $('#btnSave').attr('disabled',false); //set button enable 

		  }
	  });
	}

	function delete_college(id) {
	  if(confirm('Are you sure delete this data?'))
	  {
		  // ajax delete data to database
		  $.ajax({
			  url : "<?php echo site_url('admin/students/ajax_delete')?>/"+id,
			  type: "POST",
			  dataType: "JSON",
			  success: function(data)
			  {
				  //if success reload ajax table
				  $('#modal_form').modal('hide');
				  reload_table();
			  },
			  error: function (jqXHR, textStatus, errorThrown)
			  {
				  alert('Error deleting data');
			  }
		  });

	  }
	}
	</script>

	<!-- Bootstrap modal -->
	<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Students Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="id_sis"/> 
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="nim">NIM</label>
							<div class="col-md-9">
								<input type="text" name="nim" id="nim" placeholder="NIM" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="Nama_Mahasiswa">Name</label>
							<div class="col-md-9">
								<input type="text" name="Nama_Mahasiswa" id="Nama_Mahasiswa" placeholder="Full Name" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="kodecabang">Branch Name</label>
							<div class="col-md-9">
								<select name="kodecabang" id="kodecabang" class="form-control">
								  <option value=""> - Choose Branch Name - </option>
								  <?php foreach ($cabang as $key) { ?>
									<option value="<?=$key->kodecabang?>"> <?=$key->namacabang?> </option>
								  <?php } ?>
								</select>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="Alamat">Address</label>
							<div class="col-md-9">
								<textarea name="Alamat" id="Alamat" placeholder="Address" class="form-control"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tempat_lahir">P o B</label>
							<div class="col-md-9">
								<input type="text" name="tempat_lahir" id="tempat_lahir" placeholder="Place of Birth" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tanggal_lahir">D o B</label>
							<div class="col-md-9">
								<input name="tanggal_lahir" id="tanggal_lahir" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="TahunAngkatan">Year Generation</label>
							<div class="col-md-9">
								<input name="TahunAngkatan" id="TahunAngkatan" placeholder="YYYY" class="form-control" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="telepon">Phone Number</label>
							<div class="col-md-9">
								<input type="text" name="telepon" id="telepon" placeholder="08XXXXXXXXXX" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="kelas">Class</label>
							<div class="col-md-9">
								<input type="text" name="kelas" id="kelas" placeholder="Class Name" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tingkat">Grade</label>
							<div class="col-md-9">
								<select name="tingkat" id="tingkat" class="form-control">
								  <option value=""> - Choose Grade - </option>
									<option value="1"> Grade 1 </option>
									<option value="2"> Grade 2 </option>
									<option value="3"> Grade 3 </option>
								</select>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="status">Status</label>
							<div class="col-md-9">
								<select name="status" id="status" class="form-control">
								  <option value=""> - Choose Status - </option>
								  <option value="Aktif"> Active </option>
								  <option value="Tidak Aktif"> Not Active </option>
								  <option value="Lulus"> Graduated </option>
								</select>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!-- End Bootstrap modal -->