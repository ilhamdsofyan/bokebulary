<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Dashboard">
		<meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
		<title>LP3i - Vocabulary Online | Login</title>
		<!-- Bootstrap core CSS -->
		<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
		<!--external css-->
		<link href="<?=base_url()?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
		
		<!-- Custom styles for this template -->
		<link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">
		<link href="<?=base_url()?>assets/css/style-responsive.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/logo.png"/>
	</head>
	<body>
		<!-- **********************************************************************************************************************************************************
		MAIN CONTENT
		*********************************************************************************************************************************************************** -->
		<div id="login-page">
			<div class="container">
				
				<form class="form-login" action="<?=site_url('login/DoLogin')?>" method = "post">
					<h2 class="form-login-heading">Sign In Now !!!</h2>
					<div class="login-wrap">
						<?php
                            $userid = array(
                                'name' => "username", 
                                'id' => "username",
                                'type' => "text",
                                "placeholder" => "Username",
                                "required" => 'true',
                                "autofocus" => 'true',
                                "class" => "form-control",
                                "autocomplete" => 'off'
                            );
                            echo form_input($userid);
						?>
						<br>
						<div class="input-group">
							<?php
								$pass = array(
									'name' => "password", 
									'id' => "password",
									'type' => "password",
									"placeholder" => "Password",
									"required" => 'true',
									"autofocus" => 'true',
									"class" => "form-control"
								);
								echo form_input($pass);
							?>
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" id="see"><i id="cek" class="fa fa-eye-slash"></i></button>
							</span>
						</div>
						<br>
						<select name="role" id="role" class="form-control" required>
							<option value="" selected disabled> - Select Your Role - </option>
							<option value="biodata"> Student </option>
							<option value="profilkaryawancollege"> College Employer </option>
							<option value="profilkaryawanpoltek"> Polytechnic Employer </option>
							<option value="profilkaryawanlcc"> LCC Employer </option>
							<option value="cabang"> Supervisor </option>
						</select>
						<br>
						<a data-toggle="modal" href="#myModal" title="Help">Need Help&nbsp;<i class="fa fa-question"></i></a>
						<br><br>
						<button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
					</div>
					
					<!-- Modal -->
					<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Login Instruction</h4>
								</div>
								<div class="modal-body">
									<p>1. Type your NIM / NIK for Username. <i>Example : 140442020022</i>.</p>
									<p>2. Type your Birthday Date for Password with "YYYYMMDD" format. <i>Example : 19970404</i>.</p>
									<p>3. And last, don't forget to choose your role.</p>
									<br>
									<p>If you have any problem with your Login, make sure your NIM same with Academic data. Thank you.</p>
									<code>Any Question? Send your NIM and Name via <a href="mailto:ilham.it@lp3i.ac.id?subject=Login Error">E-mail</a></code>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
								</div>
							</div>
						</div>
					</div>
					<!-- modal -->
					
				</form>
				
			</div>
		</div>
		<!-- js placed at the end of the document so the pages load faster -->
		<script src="<?=base_url()?>assets/js/jquery.js"></script>
		<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
		<!--BACKSTRETCH-->
		<!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
		<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.backstretch.min.js"></script>
		<script>
			$.backstretch("<?=base_url()?>assets/img/login-bg.jpg", {speed: 100});
		</script>
		<script type="text/javascript">
			$("#see").mousedown(function(){
				$("#password").prop({type:"text"});
				$("#cek").removeClass("fa fa-eye-slash").addClass("fa fa-eye");
			})
			$("#see").mouseup(function(){
				$("#password").prop({type:"password"});
				$("#cek").removeClass("fa fa-eye").addClass("fa fa-eye-slash");
			})
			$("#see").mouseout(function(){
				$("#password").prop({type:"password"});
				$("#cek").removeClass("fa fa-eye").addClass("fa fa-eye-slash");
			})
		</script>
	</body>
</html>