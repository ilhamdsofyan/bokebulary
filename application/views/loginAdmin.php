<!DOCTYPE html>
<html >
	<head>
		<meta charset="UTF-8">
		<title>Vocabulary Online | Admin Login</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
		<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
		<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
		<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
		<link rel="stylesheet" href="<?=base_url('assets/admin/css/style.css')?>">
	</head>
	<body>
		<div class="container">
			<div class="info">
				<h1>Login Admin</h1>
				<span>LP3I Vocabulary Online</span>
			</div>
		</div>
		<div class="form">
			<div class="thumbnail"><img src="<?=base_url('assets/img/user0.png')?>" width="500px"/></div>
			<form class="login-form" action="<?=site_url('login/DoLoginAdmin')?>" method="post">
				<input type="text" placeholder="Username" name="username" autofocus autocomplete="off" />
				<input type="password" placeholder="Password" name="password"/>
				<button>login</button>
				<p class="message">Not Administrator? <a href="<?=base_url()?>">Go to default Login</a></p>
			</form>
		</div>
	</body>
</html>