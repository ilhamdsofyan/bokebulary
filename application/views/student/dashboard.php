<?php
	// $url = "https://www.youtube.com/feeds/videos.xml?playlist_id=PLeKHIw7zpd41hiiXKXpTVWHhL8e7J0PWk";
	// $ch = curl_init();
	// curl_setopt($ch, CURLOPT_URL, $url);
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// $xmlresponse = curl_exec($ch);
	// $RSS = simplexml_load_file($xmlresponse);
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-xs-9" id="isi">
				<h3>Welcome To Our Vocabulary Online</h3>
				<br>
        
				<div class="content-panel" style="font-weight: bold; color: #22313F;">
					<h4><i class="fa fa-angle-right"></i> English is Easy, English is Fun!!!</h4>
					<section id="unseen">
						<div style="display:none;" class="html5gallery" data-skin="gallery" data-height="272" data-responsive="true">
						<?php
							$RSS = simplexml_load_file("https://www.youtube.com/feeds/videos.xml?playlist_id=PLeKHIw7zpd41hiiXKXpTVWHhL8e7J0PWk");
							foreach ($RSS->entry as $key) {
							$judul = $key->title;
							$uri = str_replace("yt:video:", "", $key->id);
						?>
							<a href="https://www.youtube.com/embed/<?=$uri?>"><img src="https://img.youtube.com/vi/<?=$uri?>/0.jpg" alt="<?=$judul?>"></a>
						<?php } ?>
						</div>
					</section>
				</div>
        		<br>

      </div><!-- /col-xs-9 END SECTION MIDDLE -->