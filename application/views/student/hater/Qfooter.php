<!-- **********************************************************************************************************************************************************
RIGHT SIDEBAR CONTENT
*********************************************************************************************************************************************************** -->
	</div><!--/row -->
</section>
</section>
<!--main content end-->
<!--footer start-->
<footer class="site-footer">
<div class="text-center">
	Vocabulary Online LP3I Ver 2.0 | &copy; Copyright 2016. All Right Reserved - ICT Directorate <a style="color: white;font-weight: bold;" href="https://www.lp3i.ac.id/" target="_blank">LP3I</a>
	<a href="#top" class="go-top">
		<i class="fa fa-angle-up"></i>
	</a>
</div>
</footer>
<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?=base_url()?>assets/js/jquery-1.8.3.min.js"></script> 
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?=base_url()?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.sparkline.js"></script>
<script src="<?=base_url()?>assets/js/jquery-labelauty/source/jquery-labelauty.js"></script>

<!--common script for all pages-->
<script src="<?=base_url()?>assets/js/common-scripts.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter-conf.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.jCounter-0.1.4.js"></script>
</body>
</html>
<script type="text/javascript">
	$(document).ready(function() {
		// labelan
		$(".jawaban").labelauty({
			minimum_width: "88%",
			icon: false,
			same_width: true
		});
	});
</script>