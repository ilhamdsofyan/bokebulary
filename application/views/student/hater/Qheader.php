<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="LP3I Vocabulary is a good way to make students learn new vocabulary in english language.">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Ilham Dwi Sofyan, vocabulary, online, LP3I, politeknik, college, LCC, student, lecture, https://www.linkedin.com/in/ilham-sofyan-1569a1128">
    <title>LP3i | Vocabulary Online</title>
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <!--external css-->
    <link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/zabuto_calendar.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/js/gritter/css/jquery.gritter.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/lineicons/style.css')?>">
    <link href="<?=base_url('assets/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
    <link href="<?=base_url()?>assets/js/jquery-labelauty/source/jquery-labelauty.css" rel="stylesheet">
    
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/jquery.jCounter-iosl.css')?>">
    
    <!-- Custom styles for this template -->
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style-responsive.css')?>" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/logo.png"/>
  </head>
  <body>
    <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="index.html" class="logo"><b>LP3i - Vocabulary Online</b></a>
        <!--logo end-->
      </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
        <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            
            <?php
            $plj = array("001","002","003","004","010","011","038","040","045","050","066","069","070","079","099");
            if(in_array($biodata[0]->kodecabang, $plj) && $biodata[0]->foto != ""){
            ?>
              <p class="centered"><a data-toggle="modal" href="#myModal"><img src="https://sim.politekniklp3i-jkt.ac.id/AdminPendidikan/<?=$biodata[0]->foto?>" class="img-circle" width="60"></a></p>
              <p class="centered"><a href="#">
            <?php
            }elseif(!in_array($biodata[0]->kodecabang, $plj) && $biodata[0]->foto != ""){
            ?>
              <p class="centered"><a data-toggle="modal" href="#myModal"><img src="http://manajemen.lp3i.ac.id/AdminPendidikan/<?=$biodata[0]->foto?>" class="img-circle" width="60"></p>
              <p class="centered"><a href="#">
            <?php
            }else{
            ?>
              <p class="centered"><a data-toggle="modal" href="#myModal"><img src="<?=base_url('assets/img/user1.png')?>" class="img-circle" width="60"></p>
              <p class="centered"><a href="#">
            <?php  
            }
            ?>
            </a></p>
            <h5 class="centered"><?=$biodata[0]->Nama_Mahasiswa;?></h5>
            
            <li class="mt">
              <a href="">
                <i class="fa fa-bell-o"></i>
                <span> <?=$level?> </span>
              </a>
            </li>
          </ul>
          <!-- sidebar menu end-->
        </div>
      </aside>
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->