<!-- **********************************************************************************************************************************************************
RIGHT SIDEBAR CONTENT
*********************************************************************************************************************************************************** -->
<div class="col-xs-3 ds">
<!-- CALENDAR-->
<div id="calendar" class="mb">
	<div class="panel green-panel no-margin">
		<div class="panel-body">
			<div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
				<div class="arrow"></div>
				<h3 class="popover-title" style="disadding: none;"></h3>
				<div id="date-popover-content" class="popover-content"></div>
			</div>
			<div id="my-calendar"></div>
		</div>
	</div>
	</div><!-- / calendar -->
	</div><!-- /col-xs-3 -->
	</div><!--/row -->
</section>
</section>
<!--main content end-->
<!--footer start-->
<footer class="site-footer">
<div class="text-center">
	Vocabulary Online LP3I Ver 2.0 | &copy; Copyright 2016. All Right Reserved - ICT Directorate of <a style="color: white;font-weight: bold;" href="https://www.lp3i.ac.id/" target="_blank">LP3I</a>
	<a href="#top" class="go-top">
		<i class="fa fa-angle-up"></i>
	</a>
</div>
</footer>
<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?=base_url()?>assets/js/jquery-1.8.3.min.js"></script> 
<script type="text/javascript" src="<?=base_url()?>assets/html5gallery/jquery.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/html5gallery/html5gallery.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?=base_url()?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?=base_url()?>assets/js/datatables.min.js"></script>
<script src="<?=base_url()?>assets/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.sparkline.js"></script>
<!--common script for all pages-->
<script src="<?=base_url()?>assets/js/common-scripts.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter-conf.js"></script>
<!--script for this page-->
<script src="<?=base_url()?>assets/js/sparkline-chart.js"></script>
<script src="<?=base_url()?>assets/js/zabuto_calendar.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="application/javascript">
$(document).ready(function () {
	$('#html5-watermark').hide();

	$("#example").DataTable();
	$(".datepicker").datepicker({
		format : "yyyy-mm-dd",
		autoclose : true
	});
    

//Detect window size and hide calender + raise content width
var width = $(window).width();
if (width <= 1218) {
	$(".ds").hide();
	$("#isi").removeClass("col-xs-9");
	$("#isi").addClass("col-xs-12");
}
else{
	$(".ds").show();
	$("#isi").addClass("col-xs-9");
	$("#isi").removeClass("col-xs-12");
}

// $("#example").DataTable();

$("#date-popover").popover({html: true, trigger: "manual"});
$("#date-popover").hide();
$("#date-popover").click(function (e) {
$(this).hide();
});
var tanggal = new Date();
$("#my-calendar").zabuto_calendar({
	today: true
});

$("a[href='#top']").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});

});
function myNavFunction(id) {
$("#date-popover").hide();
var nav = $("#" + id).data("navigation");
var to = $("#" + id).data("to");
console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
}

//Detect window size and hide calender + raise content width
$(window).resize(function() {
	var width = $(window).width();
	if (width <= 1218) {
		$(".ds").hide();
		$("#isi").removeClass("col-xs-9");
		$("#isi").addClass("col-xs-12");
	}
	else{
		$(".ds").show();
		$("#isi").addClass("col-xs-9");
		$("#isi").removeClass("col-xs-12");
	}
})
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".clickable-row").click(function() {
			window.document.location = $(this).data("href");
		});
	});
</script>
</body>
</html>
<!-- Profil -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><?=$biodata[0]->nim?></h3>
            </div>
            <div class="modal-body form">
                <form action="<?=site_url('student/crud/update/'.$biodata[0]->nim)?>" id="form" class="form-horizontal" method="post">
                    <div class="form-body">
                    	<div class="row">
                    		<div class="col-md-8">
                    			<div class="form-group">
		                            <label class="control-label col-md-4" for="Nama_Mahasiswa">Name</label>
		                            <div class="col-md-8">
										<?php
											$nama = array("name" => "Biodata[Nama_Mahasiswa]",
															"id" => "Nama_Mahasiswa",
															"placeholder" => "Full Name",
															"class" => "form-control",
															"value" => $biodata[0]->Nama_Mahasiswa);
											echo form_input($nama);
										?>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="control-label col-md-4" for="kodecabang">Branch Name</label>
		                            <div class="col-md-8">
		                                <?php
											$cabang = array("name" => "cabang",
															"id" => "cabang",
															"class" => "form-control",
															"disabled" => "true",
															"value" => $biodata[0]->namacabang);
											echo form_input($cabang);
										?>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="control-label col-md-4" for="Alamat">Address</label>
		                            <div class="col-md-8">
		                            	<?php
											$alamat = array("name" => "alamat",
															"id" => "alamat",
															"class" => "form-control",
															"disabled" => "true",
															"style" => "height:100px;resize:none;",
															"value" => $biodata[0]->Alamat);
											echo form_textarea($alamat);
										?>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label class="control-label col-md-4" for="tempat_lahir">P o B</label>
		                            <div class="col-md-8">
		                                <?php
											$tempat = array("name" => "tempat",
															"id" => "tempat",
															"class" => "form-control",
															"disabled" => "true",
															"value" => $biodata[0]->tempat_lahir);
											echo form_input($tempat);
										?>
		                            </div>
		                        </div>
                    		</div>
                    		<div class="col-md-4">
								<?php
									$plj = array("001","002","003","004","010","011","038","040","045","050","066","069","070","079","099");
									if(in_array($biodata[0]->kodecabang, $plj) && $biodata[0]->foto != ""){
								?>
									<img src="https://sim.politekniklp3i-jkt.ac.id/AdminPendidikan/<?=$biodata[0]->foto?>" style="width:100%">
								<?php
								}elseif(!in_array($biodata[0]->kodecabang, $plj) && $biodata[0]->foto != ""){
								?>
									<img src="http://manajemen.lp3i.ac.id/AdminPendidikan/<?=$biodata[0]->foto?>" width="60">
								<?php
								}else{
								?>
									<img src="<?=base_url('assets/img/user1.png')?>" width="60">
								<?php  
								}
								?>
                    		</div>
                    	</div>
                    	<div class="row">
                    		<div class="col-md-12">
                    			<div class="form-group">
								    <label class="control-label col-md-3" for="tanggal_lahir">D o B</label>
								    <div class="col-md-9">
										<?php
											$tgllahir = array("name" => "Biodata[tanggal_lahir]",
															"id" => "tanggal_lahir",
															"placeholder" => "YYYY-MM-DD",
															"class" => "form-control datepicker",
															"value" => $biodata[0]->tanggal_lahir);
											echo form_input($tgllahir);
										?>
								    </div>
								</div>
								<div class="form-group">
								    <label class="control-label col-md-3" for="kelas">Class</label>
								    <div class="col-md-9">
								        <?php
											$kelas = array("name" => "Biodata[kelas]",
															"id" => "kelas",
															"placeholder" => "Class Name",
															"class" => "form-control",
															"value" => $biodata[0]->kelas);
											echo form_input($kelas);
										?>
								    </div>
								</div>
                    		</div>
	                	</div>
                    </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class="btn btn-success">Save Change</button>
            	</form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Buat Real Test -->
<div class="modal fade" id="realTest" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Vocabulary Real Test Agreement</h3>
            </div>
            <div class="modal-body form">
            	If you click 'yes', the <i>Real Test</i> will be begin soon. You'll not allow to stop the test, and if you stop the test you'll not able to follow the test until you clarified your problem with your supervisor/lecturer. The test will be held for 90 minutes. Good luck <i class="fa fa-smile-o"></i>.
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-success">Yes</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal

<!-- Complaint -->
<div class="modal fade" id="compl" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Feedback Form</h3>
            </div>
            <div class="modal-body form">
                <form action="<?=site_url('student/crud/complaint/'.$biodata[0]->nim)?>" id="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="perihal">Regarding</label>
                            <div class="col-md-9">
								<?php
									$perihal = array("name" => "perihal",
													"id" => "perihal",
													"placeholder" => "Complaint For ...",
													"required" => "true",
													"class" => "form-control",);
									echo form_input($perihal);
								?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="pesan">Message</label>
                            <div class="col-md-9">
                            	<?php
									$alamat = array("name" => "pesan",
													"id" => "pesan",
													"class" => "form-control",
													"required" => "true",
													"style" => "height:100px;resize:none;",);
									echo form_textarea($alamat);
								?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="gambar">Evidence / Screenshot (if any)</label>
                            <div class="col-md-9">
								<?php
									$gbr = array("name" => "gambar",
													"id" => "gambar",
													"class" => "form-control",
													"accept" => "image/*");
									echo form_upload($gbr);
								?>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
            	<button type="submit" class="btn btn-success">Submit</button>
            	</form>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->