<style type="text/css">
  .incorrect{
    background-color: #CF000F;
    color: white;
    font-weight: bold;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        
        <div class="content-panel">
          <h4><i class="fa fa-angle-right"></i> Test Scores</h4>
          <br>
          <h5>Your Score is <b><?=$score[0]->score?></b> in Level <b><?=$level?></b></h5>
          <br><br>
            <section id="unseen">
              <table class="table table-responsive" id="example">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Your Answer</th>
                    <th>Correct Answer</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=0;
                    foreach ($detailtest as $key) { 
                    $no++;
                    if ($key->jawabanuser == $key->jawabanbenar) {
                      $class = "";
                    }else{
                      $class = "incorrect";
                    }
                  ?>
                    <tr class="<?=$class?>">
                       <td><?=$no?></td>
                       <td><?=$key->soal?></td>
                       <td><?=$key->jawabanuser?></td>
                       <td><?=$key->jawabanbenar?></td>
                    </tr>
                  <?php } ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Your Answer</th>
                    <th>Correct Answer</th>
                  </tr>
                </tfoot>
              </table>
            </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->