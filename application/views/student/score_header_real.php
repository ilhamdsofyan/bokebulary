<style type="text/css">
  .clickable-row{
    cursor: pointer;
  }
  .clickable-row:hover{
    background-color: #D2D7D3;
  }
  .complete{
    color: green;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        
        <div class="content-panel">
          <h4><i class="fa fa-angle-right"></i> Test Scores</h4>
            <section id="unseen">
              <table class="table table-responsive">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Level</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=0;
                    foreach ($headertest as $key) { $no++; 
                      if ($key->status == 0) {
                        $status = "Not Complete";
                      }
                      else{
                        $status = "Complete";
                        $complete = "complete";
                        $icon = "fa fa-check";
                      }
                  ?>
                    <tr class='clickable-row' data-href='<?=site_url("student/real_score/detail/$key->smt");?>'>
                      <td><?=$no?></td>
                      <td><?=$key->tanggaltes?></td>
                      <td><?=$key->smt?></td>
                      <td class="<?=@$complete?>"><?=$status?> <i class="<?=@$icon?>"></i></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->