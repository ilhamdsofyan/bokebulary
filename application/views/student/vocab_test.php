<?php $this->load->view('student/hater/Qheader.php'); ?>
<style type="text/css">
  input[type="radio"] {
	margin-top: 1px;
	vertical-align: middle;
	cursor: pointer;
  }
  label{
	padding: 3px;
	margin-left: 20px;
	-webkit-user-select: none; /* Chrome/Safari */        
	-moz-user-select: none; /* Firefox */
	-ms-user-select: none; /* IE10+ */

	/* Rules below not implemented in browsers yet */
	-o-user-select: none;
	user-select: none;
  }
  .content-panel{
	background-color: #ECECEC;
	color: #22313F;
	padding: 5px;
  }
  table{
	border: none !important;
	border-collapse: collapse !important;
  }
  td{
	border: none !important;
  }
</style>
<?php
  $levels = substr($level,-1); 
  $no = $answered+1;
?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
	<div class="row">
	  <div class="col-xs-12">
		<h3>Welcome To Our Vocabulary Online</h3>
		<br>      
		<div class="content-panel" style="background-color: #ECECEC; color: #22313F; height: auto; flex-direction: left">
		  <h4><i class="fa fa-angle-right"></i> Test Level <?=$levels?> Good Luck <i class="fa fa-smile-o"></i><p class="pull-right" align="right" style="font-size:14px;margin-top: 8px;margin-right: 8px;">Answered <b><?=$answered?></b> from <b>1000</b></p></h4>
			  <hr>
			  <form action="<?=site_url("student/vocabulary_test/subnex/$levels")?>" method="post">
				<input type="hidden" name="idtesh" value="<?=$idtesh?>">
				<div class="form-body">

				<?php for ($i=0; $i < 5; $i++) { ?>
				<div class='row'>
				  <div class="col-sm-6 col-xs-12">
					&emsp;<label><b><?=($no+$i).'. '.ucfirst($question[$i]->soal)."<br>";?></b></label>
					<input type="hidden" name="soal[<?=$i?>]" id="<?=$i?>" value="<?=$question[$i]->idsoal?>">
					<div class="row">
					  <?php for ($x=0; $x < 2; $x++) { ?>
					  <div class="col-xs-12 col-sm-6 col-md-4">
						<?php for ($y=0; $y < 2; $y++) { if($y==1) $y++; ?>
						  <div class="form-group">
							<input class='jawaban' type="radio" name="jawaban[<?=$i?>]" value="<?=$choice[$i][$y+$x]->idsoal?>" required data-labelauty="<?=ucfirst($choice[$i][$y+$x]->jawaban)?>">
						  </div>
						<?php } ?>
					  </div>
					  <?php } ?>
					  <div class="hide-xs hide-sm col-md-4"></div>
					</div>
				  </div>

				  <?php $i++; if($i==5) { echo "<div class=\"hide-xs col-sm-6\"></div></div>"; break; } ?>
				  <div class="col-sm-6 col-xs-12">
					&emsp;<label><b><?=($no+$i).'. '.ucfirst($question[$i]->soal)."<br>";?></b></label>
					<input type="hidden" name="soal[<?=$i?>]" id="<?=$i?>" value="<?=$question[$i]->idsoal?>">
					<div class="row">
					  <?php for ($x=0; $x < 2; $x++) { ?>
					  <div class="col-xs-12 col-sm-6 col-md-4">
						<?php for ($y=0; $y < 2; $y++) { if($y==1) $y++; ?>
						  <div class="form-group">
							<input class='jawaban' type="radio" name="jawaban[<?=$i?>]" value="<?=$choice[$i][$y+$x]->idsoal?>" required data-labelauty="<?=ucfirst($choice[$i][$y+$x]->jawaban)?>">
						  </div>
						<?php } ?>
					  </div>
					  <?php } ?>
					  <div class="hide-xs hide-sm col-md-4"></div>
					</div>
				  </div>
				</div>
				<?php } ?>

				  <div align="center">
					<input type="submit" name="submit" value="Submit & Next" class="btn btn-success" title="Ready for next round :))">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#tester"><i class="fa fa-stop"></i></button>
				  </div>
				</div>
			  </form>
		</div>
	  </div><!-- /col-xs-9 END SECTION MIDDLE -->

<!-- Modal -->
<div class="modal fade" id="tester" role="dialog">
  <div class="modal-dialog">

  <!-- Modal content-->
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Confirmation</h4>
	  </div>
	  <div class="modal-body">
		<p>Are you sure want to stop this test and see your score? Your answer in this session will not be saved.</p>
	  </div>
	  <div class="modal-footer">
		<a href="<?=site_url('student/test_score/')?>" class="btn btn-success">Yes</a>
		<button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
	  </div>
	</div>

  </div>
</div>

<?php $this->load->view('student/hater/Qfooter.php'); ?>