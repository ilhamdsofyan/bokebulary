<?php $this->load->view('student/hater/header.php'); ?>
<style type="text/css">
  button{
    width:30%;
    margin-left: 40px;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        
        <div class="content-panel" style="background-color: #F5F5F5; font-weight: bold; color: #22313F;">
          <h4><i class="fa fa-angle-right"></i> Select Level</h4>
          <br>
            <section id="unseen" style="text-align: center">
              <a href="<?=site_url('student/vocabulary_test/test/1')?>">
                <button title="Beginner" class="btn btn-success">Vocabulary Test <br> Level 1</button>
              </a>
              <a href="<?=site_url('student/vocabulary_test/test/2')?>">
                <button title="Intermediate" class="btn btn-primary">Vocabulary Test <br> Level 2</button>
              </a>
              <br><br><br><br>
              <a href="<?=site_url('student/vocabulary_test/test/3')?>">
                <button title="Advanced" class="btn btn-warning">Vocabulary Test <br> Level 3</button>
              </a>
              <a href="<?=site_url('student/vocabulary_test/test/4')?>">
                <button title ="Expert" class="btn btn-danger">Vocabulary Test <br> Level 4</button>
              </a>
              <br><br><br><br>
            </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
<?php $this->load->view('student/hater/footer.php'); ?>