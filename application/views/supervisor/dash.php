<?php $this->load->view('supervisor/hater/header.php'); ?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        
        <div class="content-panel" style="background-color: #446CB3; font-weight: bold; color: white;">
          <h4><i class="fa fa-angle-right"></i> Notification</h4>
            <section id="unseen">
              <p>&emsp; You are logged in as Supervisor. You can track your members record. Beside that, you can manipulate your members data too.</p>    
            </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
<?php $this->load->view('supervisor/hater/footer.php'); ?>