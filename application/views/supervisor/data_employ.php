<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
	a{
		cursor: pointer !important;
	}
</style>
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-xs-9" id="isi">
				<h3>Welcome To Our Vocabulary Online</h3>
				<br>

				<div class="content-panel">
					<div class="col-xs-6">
						<h4><i class="fa fa-angle-right"></i> Employer's List</h4>
					</div>
					<div class="col-xs-6" align="right">
						<button class="btn btn-info btn-md" id="exim"><i class="fa fa-download"></i> Export / Import</button>
						<button class="btn btn-success btn-md" onclick="add_employ()"><i class="fa fa-plus"></i>&emsp; Add</button>
					</div>
					<section id="unseen">
						<br><br><br><br>
						<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Name</th>
									<th>D o B / Password</th>
									<th width="150px">Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th>No</th>
									<th>NIK</th>
									<th>Name</th>
									<th>D o B / Password</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</section>
				</div>
			</div><!-- /col-xs-9 END SECTION MIDDLE -->
				
<?php $this->load->view('supervisor/hater/footer.php'); ?>
<script>
$(document).ready(function() {
	//datatables
	table = $('#example').DataTable({ 

		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"order": [], //Initial no order.

		// Load data for the table's content from an Ajax source
		"ajax": {
				"url": "<?php echo site_url('supervisor/employers_data/ajax_list')?>",
				"type": "POST"
		},

		//Set column definition initialisation properties.
		"columnDefs": [
		{ 
				"targets": [ -1 ], //last column
				"orderable": false, //set not orderable
		},
		],

	});

	//DatePicker
$('.datepicker').datepicker({
	autoclose: true,
	format: "yyyy-mm-dd",
	orientation: "top auto",
	todayBtn: true,
	todayHighlight: true 
});
})

	function add_employ() {
		save_method = 'add';
		$('#form')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#modal_form').modal('show'); // show bootstrap modal
		$('.modal-title').text('Add Employs'); // Set Title to Bootstrap modal title
	}

	function edit_employ(id) {
		save_method = 'update';
		$('#form')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string

		//Ajax Load data from ajax
		$.ajax({
			url : "<?php echo site_url('supervisor/employers_data/ajax_edit/')?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data) {
				$('[name="id_pol"]').val(data.id_pol);
				$('[name="nik"]').val(data.nik);
				$('[name="nama"]').val(data.nama);
				// $('[name="kodecabang"]').val(data.kodecabang).prop('selected',true);
				$('[name="alamat"]').val(data.alamat);
				$('[name="tempat"]').val(data.tempat);
				$('[name="tanggal"]').val(data.tanggal);
				$('[name="email"]').val(data.email);
				$('[name="tglbergabung"]').val(data.tglbergabung);
				$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
				$('.modal-title').text('Edit Employs'); // Set title to Bootstrap modal title
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	}

	function reload_table() {
		table.ajax.reload(null,false); //reload datatable ajax 
	}

	function save() {
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled',true); //set button disable 
		var url;

		if(save_method == 'add') {
			url = "<?php echo site_url('supervisor/employers_data/ajax_add')?>";
		} else {
			url = "<?php echo site_url('supervisor/employers_data/ajax_update')?>";
		}

		// ajax adding data to database
		$.ajax({
			url : url,
			type: "POST",
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function(data) {
				if(data.status) { //if success close modal and reload ajax table
					$('#modal_form').modal('hide');
					reload_table();
				}

				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable 
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error adding / update data');
				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable 
			}
		});
	}

	function delete_employ(id) {
		if(confirm('Are you sure delete this data?'))
		{
			// ajax delete data to database
			$.ajax({
				url : "<?php echo site_url('supervisor/employers_data/ajax_delete')?>/"+id,
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					//if success reload ajax table
					$('#modal_form').modal('hide');
					reload_table();
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert('Error deleting data');
				}
			});
		}
	}

	$("#exim").click(function(){
		$('#form2')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#modal_exim').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export/Import Employers Data'); // Set Title to Bootstrap modal title
	})
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Employers Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="id_pol"/> 
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="nik">NIK</label>
							<div class="col-md-9">
								<input type="text" name="nik" id="nik" placeholder="NIK" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="nama">Name</label>
							<div class="col-md-9">
								<input type="text" name="nama" id="nama" placeholder="Full Name" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="alamat">Address</label>
							<div class="col-md-9">
								<textarea name="alamat" id="alamat" placeholder="Address" class="form-control"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tempat">P o B</label>
							<div class="col-md-9">
								<input type="text" name="tempat" id="tempat" placeholder="Place of Birth" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tanggal">D o B</label>
							<div class="col-md-9">
								<input name="tanggal" id="tanggal" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="email">Email</label>
							<div class="col-md-9">
								<input name="email" id="email" placeholder="example@xx.com" class="form-control" type="email">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tanggal">Join Date</label>
							<div class="col-md-9">
								<input name="tglbergabung" id="tglbergabung" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal All -->
<div class="modal fade" id="modal_exim" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title"></h3>
			</div>
			<div class="modal-body form">
				<div class="form-body">
					<div class="form-group" align="left">
						<div class="col-md-6">
							<label>Don't have the Excel format ?</label>
						</div>
						<div class="col-md-6">
							<a href="<?=base_url('assets/storage/table_format/karyawan_table_format.xlsx')?>" class="btn btn-warning" id="format_file"><i class="fa fa-file"></i>&nbsp;Click Here !</a>
						</div>
						<div class="col-lg-12"><hr></div>
					</div>
					<div class="form-group" align="left">
						<div class="col-md-6">
							<label id="text">Export Employers Data</label>
						</div>
						<div class="col-md-3">
							<a href="<?=site_url('supervisor/export/employers')?>" class="btn btn-danger">Export File</a>
						</div>
						<div class="col-lg-12">
							<hr>
						</div>
					</div>
					<form id="form2" action="<?=site_url('supervisor/import/employers')?>" class="form-horizontal" method="post" enctype="multipart/form-data">
						<input type="hidden" name="tipe" id="tipe" />
						<div class="form-group">
							<label class="control-label col-md-3" for="fupload">File to Import</label>
							<div class="col-md-6">
								<input type="file" name="fupload" id="fupload" class="form-control" accept="application/vnd.ms-excel">
								<span class="help-block">* Only Accept .XLS / Excel File.</span>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-upload"></i>&nbsp;Import File
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->