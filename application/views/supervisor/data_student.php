
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
  a{
	cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
	<div class="row">
	  <div class="col-xs-9" id="isi">
		<h3>Welcome To Our Vocabulary Online</h3>
		<br>
		<div class="content-panel">
		  <div class="col-xs-8">
			<h4><i class="fa fa-angle-right"></i> Students List</h4>
		  </div>
		  <div class="col-xs-4" align="right">
		  	<button class="btn btn-info btn-md" id="exim"><i class="fa fa-download"></i> Export / Import</button>
			<button id="adders" class="btn btn-success btn-md" onclick="add_student()"><i class="fa fa-plus"></i>&emsp; Add</button>
		  </div>
		  <section id="unseen">
			<br><br><br><br>
			  <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
				  <tr>
					<th>No</th>
					<th>NIM</th>
					<th>Name</th>
					<th>D o B / Password</th>
					<th width="150px">Action</th>
				  </tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
				  <tr>
					<th>No</th>
					<th>NIM</th>
					<th>Name</th>
					<th>D o B / Password</th>
					<th width="120px">Action</th>
				  </tr>
				</tfoot>
			  </table>
		  </section>
		</div>
	  </div><!-- /col-xs-9 END SECTION MIDDLE -->
		
<?php $this->load->view('supervisor/hater/footer.php'); ?>
<script>

$(document).ready(function() {
  var table = $("#example").DataTable({"destroy" : true});
	//datatables
	table = $("#example").DataTable({ 

		"processing": true, //Feature control the processing indicator.
		"serverSide": true, //Feature control DataTables' server-side processing mode.
		"destroy" : true, //Destroying old table
		"order": [], //Initial no order.

		// Load data for the table's content from an Ajax source
		"ajax": {
			"url": "<?=site_url()?>supervisor/students_data/ajax_list/",
			"type": "POST"
		},

		//Set column definition initialisation properties.
		"columnDefs": [
		{ 
			"targets": [ -1 ], //last column
			"orderable": false, //set not orderable
		},
		],

	});

  //DatePicker
$('.datepicker').datepicker({
	autoclose: true,
	format: "yyyy-mm-dd",
	orientation: "top auto",
	todayBtn: true,
	todayHighlight: true 
});

//YearPicker
$('.yearpicker').datepicker({
	autoclose: true,
	format: "yyyy",
	orientation: "top auto",
	todayBtn: true,
	todayHighlight: true 
});

})

  function add_student() {
	  save_method = 'add';
	  $('#form')[0].reset(); // reset form on modals
	  $('.form-group').removeClass('has-error'); // clear error class
	  $('.help-block').empty(); // clear error string
	  $('#modal_form').modal('show'); // show bootstrap modal
	  $('.modal-title').text('Add Students'); // Set Title to Bootstrap modal title
  }

  function edit_student(id) {
	  save_method = 'update';
	  $('#form')[0].reset(); // reset form on modals
	  $('.form-group').removeClass('has-error'); // clear error class
	  $('.help-block').empty(); // clear error string

	  //Ajax Load data from ajax
	  $.ajax({
		  url : "<?php echo site_url('supervisor/students_data/ajax_edit/')?>/" + id,
		  type: "GET",
		  dataType: "JSON",
		  success: function(data) {
			$('[name="id_sis"]').val(data.id_sis);
			$('[name="nim"]').val(data.nim);
			$('[name="Nama_Mahasiswa"]').val(data.Nama_Mahasiswa);
			// $('[name="kodecabang"]').val(data.kodecabang).prop('selected',true);
			$('[name="Alamat"]').val(data.Alamat);
			$('[name="tempat_lahir"]').val(data.tempat_lahir);
			$('[name="tanggal_lahir"]').val(data.tanggal_lahir);
			$('[name="telepon"]').val(data.telepon);
			$('[name="TahunAngkatan"]').val(data.TahunAngkatan);
			$('[name="kelas"]').val(data.kelas);
			$('[name="tingkat"]').val(data.tingkat);
			$('[name="status"]').val(data.status);
			$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
			$('.modal-title').text('Edit Students'); // Set title to Bootstrap modal title

		  },
		  error: function (jqXHR, textStatus, errorThrown) {
			  alert('Error get data from ajax');
		  }
	  });
  }

  function reload_table()
  {
	$("#example").DataTable().ajax.reload(null,false); //reload datatable ajax 
  }

  function save()
  {
	  $('#btnSave').text('saving...'); //change button text
	  $('#btnSave').attr('disabled',true); //set button disable 
	  var url;

	  if(save_method == 'add') {
		  url = "<?php echo site_url('supervisor/students_data/ajax_add')?>";
	  } else {
		  url = "<?php echo site_url('supervisor/students_data/ajax_update')?>";
	  }

	  // ajax adding data to database
	  $.ajax({
		  url : url,
		  type: "POST",
		  data: $('#form').serialize(),
		  dataType: "JSON",
		  success: function(data)
		  {

			  if(data.status) //if success close modal and reload ajax table
			  {
				  $('#modal_form').modal('hide');
				  reload_table();
			  }

			  $('#btnSave').text('save'); //change button text
			  $('#btnSave').attr('disabled',false); //set button enable 


		  },
		  error: function (jqXHR, textStatus, errorThrown)
		  {
			  alert('Error adding / update data');
			  $('#btnSave').text('save'); //change button text
			  $('#btnSave').attr('disabled',false); //set button enable 

		  }
	  });
  }

  function delete_student(id) {
	  if(confirm('Are you sure delete this data?'))
	  {
		  // ajax delete data to database
		  $.ajax({
			  url : "<?php echo site_url('supervisor/students_data/ajax_delete')?>/"+id,
			  type: "POST",
			  dataType: "JSON",
			  success: function(data)
			  {
				  //if success reload ajax table
				  $('#modal_form').modal('hide');
				  reload_table();
			  },
			  error: function (jqXHR, textStatus, errorThrown)
			  {
				  alert('Error deleting data');
			  }
		  });

	  }
  }

	$("#exim").click(function(){
		$('#form2')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#modal_exim').modal('show'); // show bootstrap modal
		$('.modal-title').text('Export/Import Students Data'); // Set Title to Bootstrap modal title
	})
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title">Students Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="id_sis"/> 
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3" for="nim">NIM</label>
							<div class="col-md-9">
								<input type="text" name="nim" id="nim" placeholder="NIM" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="Nama_Mahasiswa">Name</label>
							<div class="col-md-9">
								<input type="text" name="Nama_Mahasiswa" id="Nama_Mahasiswa" placeholder="Full Name" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="Alamat">Address</label>
							<div class="col-md-9">
								<textarea name="Alamat" id="Alamat" placeholder="Address" class="form-control"></textarea>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tempat_lahir">P o B</label>
							<div class="col-md-9">
								<input type="text" name="tempat_lahir" id="tempat_lahir" placeholder="Place of Birth" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tanggal_lahir">D o B</label>
							<div class="col-md-9">
								<input name="tanggal_lahir" id="tanggal_lahir" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="TahunAngkatan">Year Generation</label>
							<div class="col-md-9">
								<input name="TahunAngkatan" id="TahunAngkatan" placeholder="YYYY" class="form-control" type="text" autocomplete="off">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="telepon">Phone Number</label>
							<div class="col-md-9">
								<input type="text" name="telepon" id="telepon" placeholder="08XXXXXXXXXX" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="kelas">Class</label>
							<div class="col-md-9">
								<input type="text" name="kelas" id="kelas" placeholder="Class Name" class="form-control">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="tingkat">Grade</label>
							<div class="col-md-9">
								<select name="tingkat" id="tingkat" class="form-control">
								  <option value=""> - Choose Grade - </option>
									<option value="1"> Grade 1 </option>
									<option value="2"> Grade 2 </option>
									<option value="3"> Grade 3 </option>
								</select>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3" for="status">Status</label>
							<div class="col-md-9">
								<select name="status" id="status" class="form-control">
								  <option value=""> - Choose Status - </option>
								  <option value="Aktif"> Active </option>
								  <option value="Tidak Aktif"> Not Active </option>
								  <option value="Lulus"> Graduated </option>
								</select>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- Bootstrap modal All -->
<div class="modal fade" id="modal_exim" role="dialog">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 class="modal-title"></h3>
	  </div>
	  <div class="modal-body form">
		<div class="form-body">
		  <div class="form-group" align="left">
			<div class="col-md-6">
			  <label>Don't have the Excel format ?</label>
			</div>
			<div class="col-md-6">
			  <a href="<?=base_url('assets/storage/table_format/mahasiswa_table_format.xlsx')?>" class="btn btn-warning" id="format_file"><i class="fa fa-file"></i>&nbsp;Click Here !</a>
			</div>
			<div class="col-lg-12"><hr></div>
		  </div>
		  <div class="form-group" align="left">
			<div class="col-md-6">
			  <label id="text">Export Students Data</label>
			</div>
			<div class="col-md-3">
			  <a href="<?=site_url('supervisor/export/students')?>" class="btn btn-danger">Export File</a>
			</div>
			<div class="col-lg-12">
			  <hr>
			</div>
		  </div>
		  <form id="form2" action="<?=site_url('supervisor/import/students')?>" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" name="tipe" id="tipe" />
			<div class="form-group">
			  <label class="control-label col-md-3" for="fupload">File to Import</label>
			  <div class="col-md-6">
				<input type="file" name="file" id="file" class="form-control" accept="application/vnd.ms-excel">
				<span class="help-block">* Only Accept .XLS / Excel File.</span>
			  </div>
			  <div class="col-md-3">
				<button type="submit" class="btn btn-success">
				  <i class="fa fa-upload"></i>&nbsp;Import File
				</button>
			  </div>
			</div>
		  </form>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->