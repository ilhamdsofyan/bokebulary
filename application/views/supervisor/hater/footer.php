<!-- **********************************************************************************************************************************************************
RIGHT SIDEBAR CONTENT
*********************************************************************************************************************************************************** -->
<div class="col-xs-3 ds">
<!-- CALENDAR-->
<div id="calendar" class="mb">
	<div class="panel green-panel no-margin">
		<div class="panel-body">
			<div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
				<div class="arrow"></div>
				<h3 class="popover-title" style="disadding: none;"></h3>
				<div id="date-popover-content" class="popover-content"></div>
			</div>
			<div id="my-calendar"></div>
		</div>
	</div>
	</div><!-- / calendar -->
	</div><!-- /col-xs-3 -->
	</div><!--/row -->
</section>
</section>
<!--main content end-->
<!--footer start-->
<footer class="site-footer">
<div class="text-center">
	Vocabulary Online LP3I Ver 2.0 | &copy; Copyright 2016. All Right Reserved - ICT Directorate <a style="color: white;font-weight: bold;" href="https://www.lp3i.ac.id/" target="_blank">LP3I</a>
	<a href="#top" class="go-top">
		<i class="fa fa-angle-up"></i>
	</a>
</div>
</footer>
<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="<?=base_url()?>assets/js/jquery-1.8.3.min.js"></script> 
<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?=base_url()?>assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?=base_url()?>assets/js/datatables.min.js"></script>
<script src="<?=base_url()?>assets/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.sparkline.js"></script>
<!--common script for all pages-->
<script src="<?=base_url()?>assets/js/common-scripts.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/gritter-conf.js"></script>
<!--script for this page-->
<script src="<?=base_url()?>assets/js/sparkline-chart.js"></script>
<script src="<?=base_url()?>assets/js/zabuto_calendar.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script type="application/javascript">
$(document).ready(function () {

//Detect window size and hide calender + raise content width
var width = $(window).width();
if (width <= 1218) {
	$(".ds").hide();
	$("#isi").removeClass("col-xs-9");
	$("#isi").addClass("col-xs-12");
}
else{
	$(".ds").show();
	$("#isi").addClass("col-xs-9");
	$("#isi").removeClass("col-xs-12");
}

// $("#example").DataTable();

$("#date-popover").popover({html: true, trigger: "manual"});
$("#date-popover").hide();
$("#date-popover").click(function (e) {
$(this).hide();
});
$("#my-calendar").zabuto_calendar({
	today : true
});

$("a[href='#top']").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});

});
function myNavFunction(id) {
$("#date-popover").hide();
var nav = $("#" + id).data("navigation");
var to = $("#" + id).data("to");
console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
}

//Detect window size and hide calender + raise content width
$(window).resize(function() {
	var width = $(window).width();
	if (width <= 1218) {
		$(".ds").hide();
		$("#isi").removeClass("col-xs-9");
		$("#isi").addClass("col-xs-12");
	}
	else{
		$(".ds").show();
		$("#isi").addClass("col-xs-9");
		$("#isi").removeClass("col-xs-12");
	}
})
</script>
</body>
</html>