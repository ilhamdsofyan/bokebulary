<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="LP3I Vocabulary is a good way to make students learn new vocabulary in english language.">
	<meta name="author" content="Dashboard">
	<meta name="keyword" content="Ilham Dwi Sofyan, vocabulary, online, LP3I, politeknik, college, LCC, student, lecture, https://www.linkedin.com/in/ilham-sofyan-1569a1128">
	<title>LP3i | Vocabulary Online [Supervisor Page]</title>
	<!-- Bootstrap core CSS -->
	<link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
	<!--external css-->
	<link href="<?=base_url('assets/font-awesome/css/font-awesome.css')?>" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/zabuto_calendar.css')?>">
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/js/gritter/css/jquery.gritter.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('assets/lineicons/style.css')?>">
	<link href="<?=base_url('assets/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
	<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.bootstrap.min.css" rel="stylesheet">
	
	<!-- Custom styles for this template -->
	<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
	<link href="<?=base_url('assets/css/style-responsive.css')?>" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="<?=base_url()?>assets/img/logo.png"/>
  </head>
  <body>
	<section id="container" >
	  <!-- **********************************************************************************************************************************************************
	  TOP BAR CONTENT & NOTIFICATIONS
	  *********************************************************************************************************************************************************** -->
	  <!--header start-->
	  <header class="header black-bg">
		<div class="sidebar-toggle-box">
		  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
		</div>
		<!--logo start-->
		<a href="index.html" class="logo"><b>LP3i - Vocabulary Online</b></a>
		<!--logo end-->
		<div class="nav notify-row" id="top_menu">
		  <!--  notification start -->
		  <ul class="nav top-menu">
			<!-- settings start -->
			<li class="dropdown">
			  <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
				<i class="fa fa-cog"></i>
			  </a>
			  <ul class="dropdown-menu extended tasks-bar">
				<li>
					<a href="#">
						<i class="fa fa-warning"></i>&emsp; Complaint or Advice
						<span class="badge bg-theme">5</span>
					</a>
				</li>
			  </ul>
			</li>
			<!-- inbox dropdown end -->
		  </ul>
		  <!--  notification end -->
		</div>
		<div class="top-menu">
		  <ul class="nav pull-right top-menu">
			<li><a class="logout" href="<?=site_url('login/logout')?>">Logout</a></li>
		  </ul>
		</div>
	  </header>
	  <!--header end-->
	  
	  <!-- **********************************************************************************************************************************************************
	  MAIN SIDEBAR MENU
	  *********************************************************************************************************************************************************** -->
	  <!--sidebar start-->
	  <aside>
		<div id="sidebar"  class="nav-collapse ">
		  <!-- sidebar menu start-->
		  <ul class="sidebar-menu" id="nav-accordion">
			<?php if (!empty("https://sim.politekniklp3i-jkt.ac.id/AdministratorCabang/".$anodite[0]['gambarkampus'])) { ?>
			  <p class="centered"><img src="https://sim.politekniklp3i-jkt.ac.id/AdministratorCabang/<?=$anodite[0]['gambarkampus']?>" class="img-circle" width="60"></p>
			<?php } else { ?>
			  <p class="centered"><img src="<?=base_url('assets/img/user3.png')?>" class="img-circle" width="60"></p>
			<?php } ?>
			<h5 class="centered"><?=ucfirst($anodite[0]['namacabang'])?></h5>
			<li class="mt">
			  <a href="<?=site_url('supervisor/dashboard/')?>">
				<i class="fa fa-dashboard"></i>
				<span>Dashboard</span>
			  </a>
			</li>
			<li>
			  <a href="<?=site_url('supervisor/employers_data/')?>">
				<i class="fa fa-th"></i>
				<span>Employer's Data</span>
			  </a>
			</li>
			<li>
			  <a href="<?=site_url('supervisor/students_data/')?>">
				<i class="fa fa-th"></i>
				<span>Student's Data</span>
			  </a>
			</li>
			<li class="sub-menu">
			  <a href="javascript:;" >
				<i class="fa fa-trophy"></i>
				<span>Employer's Score</span>
			  </a>
			  <ul class="sub">
				<li><a href="<?=site_url('supervisor/employers_score/')?>">Vocabulary Test</a></li>
				<li><a href="<?=site_url('supervisor/employers_real_score/')?>">Real Vocabulary Test</a></li>
			  </ul>
			</li>
			<li class="sub-menu">
			  <a href="javascript:;" >
				<i class="fa fa-trophy"></i>
				<span>Student's Score</span>
			  </a>
			  <ul class="sub">
				<li><a href="<?=site_url('supervisor/students_score/')?>">Vocabulary Test</a></li>
				<li><a href="<?=site_url('supervisor/students_real_score/')?>">Real Vocabulary Test</a></li>
			  </ul>
			</li>
		  </ul>
		  <!-- sidebar menu end-->
		</div>
	  </aside>
	  <!--sidebar end-->
	  
	  <!-- **********************************************************************************************************************************************************
	  MAIN CONTENT
	  *********************************************************************************************************************************************************** -->