<?php $this->load->view('supervisor/hater/header.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
<style type="text/css">
  a{
    cursor: pointer !important;
  }
</style>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-xs-9" id="isi">
        <h3>Welcome To Our Vocabulary Online</h3>
        <br>
        <div class="content-panel">
          <div class="col-xs-8">
            <h4>Employe's Score</h4>
          </div>
          <section id="unseen">
            <br><br><br><br>
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Level</th>
                  <th>Answered</th>
                  <th>Score</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>NIK</th>
                  <th>Name</th>
                  <th>Level</th>
                  <th>Answered</th>
                  <th>Score</th>
                </tr>
              </tfoot>
            </table>
          </section>
        </div>
      </div><!-- /col-xs-9 END SECTION MIDDLE -->
        
<?php $this->load->view('supervisor/hater/footer.php'); ?>
<script>
  $(document).ready(function() {
    var table = $("#example").DataTable({"destroy" : true});
      //datatables
      table = $("#example").DataTable({ 

          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "destroy" : true, //Destroying old table
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "<?=site_url()?>supervisor/employers_score/ajax_list/",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
          ],
      });
  })
</script>